#include "SDL.h"
#include "clashloyal.h"
#include <stdio.h>
#include <stdlib.h>

//typedef Tunite* ** TplateauJeu;

TplateauJeu AlloueTab2D(int largeur, int hauteur){
    TplateauJeu jeu;
    jeu = (Tunite***)malloc(sizeof(Tunite**)*largeur);
    for (int i=0;i<largeur;i++){
        jeu[i] = (Tunite**)malloc(sizeof(Tunite*)*hauteur);
    }
    return jeu;  //tab2D contenant des pointeurs
}

void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur){
    for (int i=0;i<largeur;i++){
        for (int j=0;j<hauteur;j++){
            jeu[i][j] = NULL;
        }
    }

    //POUR LA DEMO D'AFFICHAGE UNIQUEMENT, A SUPPRIMER
    //(les tours ici ne sont pas li�es aux listes des unit�s de vos joueurs)
    jeu[5][3]=creeTour(5,3);
    jeu[5][1]=creeTourRoi(5,1);
    jeu[5][15]=creeTour(5,15);
    jeu[5][17]=creeTourRoi(5,17);
    //FIN DEMO AFFICHAGE
}

void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur){
    //pour un affichage sur la console, en relation avec enum TuniteDuJeu
    const char* InitialeUnite[6]={"T", "R", "A", "C", "D", "G"};

    printf("\n");
    for (int j=0;j<hauteur;j++){
        for (int i=0;i<largeur;i++){
                // A ne pas donner aux etudiants
            if (jeu[i][j] != NULL){
                    printf("%s",InitialeUnite[jeu[i][j]->nom]);
            }
            else printf(" ");  //cad pas d'unit� sur cette case
        }
        printf("\n");
    }
}

bool peutAttaquerCible(Tunite* uniteAtk, Tunite* uniteDef){
    int range = getPorteeUnite(uniteAtk);
    Tcible cibleUniteAtk = getCibleAttaquableUnite(uniteAtk);
    Tcible positionUniteDef = getMaPositionUnite(uniteDef);
    bool positionAttaquable = false;
    if (cibleUniteAtk == solEtAir){
        positionAttaquable = true;
    } else {
        positionAttaquable = (cibleUniteAtk == positionUniteDef);
    }
    return (distEntreUnite(uniteAtk, uniteDef) <= range) && positionAttaquable;
}

//Creation Unite
Tunite *creeTour(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tour);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 500);
    setVitesseAttaqueUnite(nouv, 1.0);
    setDegatsUnite(nouv, 100);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeTourRoi(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tourRoi);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 800);
    setVitesseAttaqueUnite(nouv, 1.2);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 4);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeArcher(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, archer);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.7);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 1);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeChevalier(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, chevalier);
    setCibleAttaquableUnite(nouv, sol);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 400);
    setVitesseAttaqueUnite(nouv, 1.5);
    setDegatsUnite(nouv, 250);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeDragon(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, dragon);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 200);
    setVitesseAttaqueUnite(nouv, 1.1);
    setDegatsUnite(nouv, 70);
    setPorteeUnite(nouv, 2);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeGargouille(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, gargouille);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.6);
    setDegatsUnite(nouv, 90);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 3);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}


bool aPlusDePV(Tunite * UniteA, Tunite * UniteB){
    return (getPointsDeVieUnite(UniteA) > getPointsDeVieUnite(UniteB));
}

bool attaquePlusVite(Tunite * UniteA, Tunite * UniteB){
    return (getVitesseAttaqueUnite(UniteA) < getVitesseAttaqueUnite(UniteB));
}

bool faitPlusDeDmg(Tunite * UniteA, Tunite * UniteB){
    return (getDegatsUnite(UniteA) > getDegatsUnite(UniteB));
}

bool aPlusDePortee(Tunite * UniteA, Tunite * UniteB){
    return (getPorteeUnite(UniteA) > getPorteeUnite(UniteB));
}

bool coutePlusCher(Tunite * UniteA, Tunite * UniteB){
    return (getCoutEnElixirUnite(UniteA) > getCoutEnElixirUnite(UniteB));
}


bool isUniteInList(Tunite * Unite, TListePlayer playerList){
    TListePlayer courant = getptrFirstCell(playerList);
    int x = getPosXUnite(Unite);
    int y = getPosYUnite(Unite);
    Tunite* dataCurr;
    while (!listeVide(courant)){
        dataCurr = getPtrData(courant);
        if (getPosXUnite(dataCurr) == x && getPosYUnite(dataCurr) == y){
            return true;
        }
        courant = getptrNextCell(courant);
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase combat ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TListePlayer estAPortee(Tunite * UnitePlayer, TListePlayer playerList, TplateauJeu jeu){
    int range = UnitePlayer->portee;
    int x = getPosXUnite(UnitePlayer);
    int y = getPosYUnite(UnitePlayer);
    int i, j;
    Tunite* currUnite = NULL;
    TListePlayer listeCible = NULL;
    for (i = x - range; i <= x + range; i++){
        for (j = y - range; i <= y + range; i++){
            if (jeu[i][j] != NULL){
                currUnite = jeu[i][j];
                if (!isUniteInList(currUnite, playerList)){
                    listeCible = ajoutEnTetePtr(listeCible, currUnite);
                }
            }
        }
    }
    return listeCible;
}

void supprUniteMortes(TListePlayer* playerList, TplateauJeu jeu){
    TListePlayer courant = *playerList;
    while (!listeVide(courant)){
        if (getPointsDeVieUnite(getPtrData(courant)) <= 0){
            if (courant == *playerList)
                *playerList = getptrNextCell(*playerList);
            jeu[getPosXUnite(getPtrData(courant))][getPosYUnite(getPtrData(courant))] = NULL;
            courant = suppCaseListe(courant);
        } else {
            courant = getptrNextCell(courant);
        }
    }
    *playerList = getptrFirstCell(*playerList);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase combat //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase creation //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Tunite *AcheteUnite(int *elixirEnStockduJoueur){

    if(*elixirEnStockduJoueur <= 0){
        // Le joueur n'a pas d'�lixir et ne peut donc pas acheter d'unite
        return NULL;
    }
    //le joueur a assez d'�lixir pour acheter une unite
    else{
        // valeur al�atoire pour d�cider si l'ia achete ce tour
        int randint;
        randint = rand()%2; // 1 = achat | 0 = pas d'achat

        //l'ia achete ce tour
        if(randint == 1){

            Tunite *nouvelleUnite = (Tunite*)malloc(sizeof(Tunite));

            if(*elixirEnStockduJoueur >= 4){
                randint = rand()%4 +1; // pour choisir une unit�
            }
            else{   // si l'ia n'a pas assez d'elixir pour acheter certaines unit�s
                randint = rand()%(4-(*elixirEnStockduJoueur)) +1;
            }

            switch(randint) {
                case 1:
                    //creation de l'unit� choisit al�atoirement
                    nouvelleUnite = creeGargouille(0, 0);
                    //on enl�ve de l'elixir � l'ia en fonction de l'unite achet�e
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 1;
                    break;
                case 2:
                    nouvelleUnite = creeArcher(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 2;
                    break;
                case 3:
                    nouvelleUnite = creeDragon(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 3;
                    break;
                case 4:
                    nouvelleUnite = creeChevalier(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 4;
                    break;
                default:
                    printf("Erreur AcheteUnite : le nombre al�atoire invalide !\n");
                    exit(EXIT_FAILURE);
            }

            return nouvelleUnite;
        }
        else{
            //l'ia n'achete pas ce tour (randint == 0)
            return NULL;
        }


    }
}

Tunite *InitAndFindFreePos_HAUT(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 5;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 1 <= i <= 9 et place libre non trouv�e
    while(i >= 1 && i <= 9 && !find){

        // changement de colonne (Order X : 4->6->3->7->2->8->1->9)
        if(side == 1){
            // switch sur le c�t� droit
            change_sideN = change_sideN -2;
            i = i + change_sideN;
            side++;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
            side--;
        }
        // Evolution change_sideN : 1->-1>-3->-5->-7
        // Evolution change_sidep : 0->2->4->6->8 (+)

        while(j >= 3 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(Unite_A_Placer, i); // on change les coord
                setPosXUnite(Unite_A_Placer, j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j--; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

Tunite *InitAndFindFreePos_BAS(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 13;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 1 <= i <= 9 et place libre non trouv�e
    while(i >= 1 && i <= 9 && !find){

        // changement de colonne (Order X (i) : 4->6->3->7->2->8->1->9)
        if(side == 1){
            // switch sur le c�t� droite
            change_sideN = change_sideN -2;
            i = i + change_sideN;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
        }
        // Evolution change_sideN : 1->-1>-3->-5->-7
        // Evolution change_sidep : 0->2->4->6->8

        while(j <= 15 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(Unite_A_Placer, i); // on change les coord
                setPosXUnite(Unite_A_Placer, j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j++; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

// On ne renvoit rien et on ajoute en t�te
//--> le pointeur donn� ne sera plus le 1er donc utilis� getptrFirstCell() ?
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite){
    TListePlayer courant = *player;

    // on ajoute la nouvelle unit� que si la liste n'est pas vide (fin de partie)
    if(!listeVide(courant)){
        ajoutEnTete(courant, *nouvelleUnite);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase creation ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase elixir ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int elixirGiven(){
    int randint;
    randint = rand()%3 + 1;
    return randint;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase elixir //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase Deplacement ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

Priorit� d�placement :
-> Liste vide ?
-> unit� qui peut se d�placer ?
    -> Can_moove(unite)
    ->�vite que comparer entre une unite deplacable et tour

===> attaquePlusVite

-----------------------
D�placement :
-> S'il n'y a pas des unite � porter
-> Pas de d�placement vers l'arri�re
    -> attaque derri�re possible mais pas de d�placement

Si vers la gauche :
-> d�placement vers diagonale droite

Si au centre :
-> d�placement central (en fonction de l'emplacement du joueur)

Si vers la gauche :
-> d�placement vers diagonale gauche


*/

// D�termine si l'unit� � de la vitesse de d�placement
bool Can_move(Tunite *unite){
    return (getVitesseDeplacementUnite(unite) <= 0);
}

/*
// D�termine qu'elle unite se d�place entre 2 unite
int Who_moove(TListePlayer player1_HAUT, TListePlayer player2_BAS){
    // donne le joueur qui se d�place
    int ia_player; // 1 : joueur1_HAUT | 2 : joueur2_BAS | 0 : aucun

    if(!listeVide(player1_HAUT)){
        if(!listeVide(player2_BAS)){
            if(Can_moove(getPtrData(player1_HAUT))){
                //Priorit� de d�placement aux unit�s qui tape le plus rapidement
                if(!attaquePlusVite(getPtrData(player1_HAUT), getPtrData(player2_BAS))){
                    // unite player1_HAUT tape plus vite que unite player2_BAS
                    ia_player = 1;
                }else{
                    // l'unite player2_BAS tape la plus vite
                    ia_player = 2;
                }
            }
        }else{
            //liste joueur2_BAS vide -> on fait jouer le joueur1_HAUT
            ia_player = 1;
        }
    }else{
        if(Can_moove(getPtrData(player2_BAS))){
            //
            ia_player = 2;
        }
        else{
            //liste player1_HAUT vide et unite player2_BAS ne peut pas se d�placer
            return 0;
        }

    }
}
*/

bool aUneCible(Tunite* uniteAtk, TListePlayer playerDef){   //retourne true si l'uniteAtk est a portee d'au moins 1 unite adverse
    TListePlayer listeCourant = playerDef;
    Tunite * uniteCourant;
    while (!listeVide(listeCourant)){
        uniteCourant = getPtrData(listeCourant);
        if (peutAttaquerCible(uniteAtk, uniteCourant)){
            return true;
        } else {
            listeCourant = getptrNextCell(listeCourant);
        }
    }
    return false;
}

int is_Free_deplacement_X(TplateauJeu jeu, int unitePosX, int unitePosY, int deplace){
    if(jeu[unitePosX+deplace][unitePosY] == NULL){
        //la case est libre
        return deplace;
    }else{
        //la case n'est pas libre
        return 0;
    }
}

int is_Free_deplacement_Y(TplateauJeu jeu, int unitePosX, int unitePosY, int deplace){
    if(jeu[unitePosX][unitePosY+deplace] == NULL){
        //la case est libre
        return deplace;
    }else{
        //la case n'est pas libre
        return 0;
    }
}


void Deplacement_Unite_J_HAUT(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef){

        moveSpeed Nbr_Case_deplace; // vitesse deplacement de l'unite

        // coordonn�es de l'unite � d�placer
        int unitePosX = getPosXUnite(unite_deplace), unitePosY= getPosYUnite(unite_deplace);
        //int deplace_X = 0, deplace_Y = 0; //enregistrer des d�placements

        Nbr_Case_deplace = getVitesseDeplacementUnite(unite_deplace);

        for(int i = 0; i < Nbr_Case_deplace; i++){

            if(aUneCible(unite_deplace, playerDef)){
                //il n'y a pas de cible � portee
                if(0 <= unitePosX && unitePosX < 5){
                    //l'unite se trouve du c�t� gauche
                    if(unitePosY%2 == 0){ // Y pair : d�placement = X+1
                        unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, 1);
                    }else{ // Y impair : d�placement = Y+1
                        unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                    }
                }
                else{
                    if(5 < unitePosX && unitePosX <= 10){
                        //l'unite se trouve du c�t� droit
                        if(unitePosY%2 == 0){ // Y pair : d�placement = X-1
                            unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, -1);
                        }else{ //Y impair : d�placement = Y+1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                        }

                    }else{
                        if(unitePosX == 5){
                            //l'unite se trouve au centre : d�placement = Y+1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                        }else{
                            printf("Erreur coord X unite hors plateau, val X = %d", unitePosX);
                        }
                    }
                }
            }
            else{//il y a une cible � portee -> Arret boucle/deplacement -> pas de d�placement
                i += 20;
            }
        }//Fin de la boucle

        //on re-v�rifie que la case est bien vide
        if(jeu[unitePosX][unitePosY] == NULL && Nbr_Case_deplace > 0){
            //la case est bien vide

            //on modifie les valeur X et Y de l'unite
            setPosXUnite(unite_deplace, unitePosX); //on change la valeur de X
            setPosYUnite(unite_deplace, unitePosY); //on change la valeur de Y

            // on met � null l'ancien emplacement dans jeu
            jeu[getPosXUnite(unite_deplace)][getPosYUnite(unite_deplace)] = NULL;

            //on rajoute le pointeur de l'unite dans la nouvelle case
            jeu[unitePosX][unitePosY] = unite_deplace;
        }
        else{

            printf("\n ERREUR Deplacement_Unite_J_HAUT\n");
        }
}

void Deplacement_Unite_J_BAS(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef){

        moveSpeed Nbr_Case_deplace; // vitesse deplacement de l'unite

        // coordonn�es de l'unite � d�placer
        int unitePosX = getPosXUnite(unite_deplace), unitePosY= getPosYUnite(unite_deplace);
        //int deplace_X = 0, deplace_Y = 0; //enregistrer des d�placements

        Nbr_Case_deplace = getVitesseDeplacementUnite(unite_deplace);

        for(int i = 0; i < Nbr_Case_deplace; i++){

            if(aUneCible(unite_deplace, playerDef)){
                //il n'y a pas de cible � portee
                if(0 <= unitePosX && unitePosX < 5){
                    //l'unite se trouve du c�t� gauche
                    if(unitePosY%2 == 0){ // Y pair : d�placement = X+1
                        unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, 1);
                    }else{ // Y impair : d�placement = Y-1
                        unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                    }
                }
                else{
                    if(5 < unitePosX && unitePosX <= 10){
                        //l'unite se trouve du c�t� droit
                        if(unitePosY%2 == 0){ // Y pair : d�placement = X-1
                            unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, -1);
                        }else{ //Y impair : d�placement = Y-1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                        }

                    }else{
                        if(unitePosX == 5){
                            //l'unite se trouve au centre : d�placement = Y-1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                        }else{
                            printf("Erreur coord X unite hors plateau, val X = %d", unitePosX);
                        }
                    }
                }
            }
            else{//il y a une cible � portee -> Arret boucle/deplacement -> pas de d�placement
                i += 20;
            }
        }//Fin de la boucle

        //on re-v�rifie que la case est bien vide
        if(jeu[unitePosX][unitePosY] == NULL && Nbr_Case_deplace > 0){
            //la case est bien vide

            //on modifie les valeur X et Y de l'unite
            setPosXUnite(unite_deplace, unitePosX); //on change la valeur de X
            setPosYUnite(unite_deplace, unitePosY); //on change la valeur de Y

            // on met � null l'ancien emplacement dans jeu
            jeu[getPosXUnite(unite_deplace)][getPosYUnite(unite_deplace)] = NULL;

            //on rajoute le pointeur de l'unite dans la nouvelle case
            jeu[unitePosX][unitePosY] = unite_deplace;
        }
        else{
            printf("\n ERREUR Deplacement_Unite_J_BAS\n");
        }
}

void Deplacement_Unite(TListePlayer player_1_HAUT, TListePlayer player_2_BAS, TplateauJeu jeu){

    int ia_player; // donne le joueur qui se d�place -> 1 = Joueur HAUT || 2 = j BAS
    //moveSpeed Nbr_Case_deplace; // moveSpeed d'une unite
    //int unitePosX, unitePosY; // coordonn�es de l'unite � d�placer
    Tunite *unite_deplace; //pointeur de l'unite qui se d�place
    Tunite *unite_actual_1_HAUT; //courant unite du joueur 1
    Tunite *unite_actual_2_BAS; // courant unite du joueur 2
    TListePlayer courant_player1_HAUT = player_2_BAS;
    TListePlayer courant_player2_BAS = player_1_HAUT;

    while(!listeVide(courant_player1_HAUT) || !listeVide(courant_player2_BAS)){

        unite_actual_1_HAUT = getPtrData(courant_player1_HAUT);
        unite_actual_2_BAS = getPtrData(courant_player2_BAS);

        if(!listeVide(courant_player1_HAUT)){
            if(!listeVide(courant_player2_BAS)){
                if(Can_move(unite_actual_1_HAUT)){
                    //l'unite joueur1_HAUT peut se d�placer

                    //Priorit� de d�placement aux unit�s qui tape le plus rapidement
                    if(!attaquePlusVite(unite_actual_1_HAUT, unite_actual_2_BAS)){
                        // unite player1_HAUT tape plus vite que unite player2_BAS
                        unite_deplace = unite_actual_1_HAUT;
                        courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                        ia_player = 1;
                    }else{
                        // l'unite player2_BAS tape la plus vite
                        unite_deplace = unite_actual_2_BAS;
                        courant_player2_BAS = getptrNextCell(courant_player2_BAS);
                        ia_player = 2;
                    }
                }else{ // l'unite joueur1_HAUT ne peut pas se d�placer
                    // �vite de comparer en boucle une Tour avec une unite d�placable
                    courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                    unite_deplace = unite_actual_2_BAS;
                    courant_player2_BAS = getptrNextCell(courant_player2_BAS);
                    ia_player = 2;
                }
            }else{
                //liste joueur2_BAS vide -> on fait jouer le joueur1_HAUT
                unite_deplace = unite_actual_1_HAUT;
                courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                ia_player =1;
            }
        }else{
            //unite player2_BAS se d�place (liste joueur1_HAUT VIDE)
            unite_deplace = unite_actual_2_BAS;
            courant_player2_BAS = courant_player2_BAS->suiv;
            ia_player = 2;
        }

        /*
        D�placement :
            -> Uniquement s'il n'y a pas d'unite � porter

            -> Pas de d�placement vers l'arri�re (ignore unite derri�re non � portee)

            Si vers la gauche : (0 <= X < 5)
                -> d�placement vers diagonale droite (droite ou centre)
                    -> Si Y pair (Y%2 = 0)      -> d�placement vers la droite (X+1)
                    -> Si Y impair (Y%2 != 0)   -> d�placement vers le centre (Y+1 ou Y-1)
                                                    -> en fonction du joueur
                                                        ->J Haut Y+1
                                                        ->J BAS Y-1

            Si au centre : (X = 5)
                -> d�placement central
                 -> en fonction du joueur
                    ->Haut Y+1
                    ->BAS Y-1
                -> Si bloqu� entre 2 tour ?

            Si vers la gauche : (5 < X <= 10)
                -> d�placement vers diagonale gauche (gauche ou centre)
                    -> Si Y pair (Y%2 == 0)      -> d�placement vers la gauche (X-1)
                    -> Si Y impair (Y%2 != 0)   -> d�placement vers le centre (Y+1 ou Y-1)
                                                    -> en fonction du joueur (haut/bas)
                                                     -> en fonction du joueur
                                                        ->J Haut Y+1
                                                        ->J BAS Y-1
        */

        /*/--------------------------- D�placement ------------------------------  /*/

        //Nbr_Case_deplace = getVitesseDeplacementUnite(unite_deplace);

        if(ia_player == 1){
            //le joueur1_HAUT se d�place
            Deplacement_Unite_J_HAUT(unite_deplace, jeu, player_2_BAS);
            courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
        }
        else{
            //le joueur2_BAS se d�place
            Deplacement_Unite_J_BAS(unite_deplace, jeu, player_1_HAUT);
            courant_player2_BAS = getptrNextCell(courant_player2_BAS);
        }


    }//////-- Fin de la boucle --//////

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase Deplacement /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

