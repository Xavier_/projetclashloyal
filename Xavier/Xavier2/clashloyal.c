#include "SDL.h"
#include "clashloyal.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//typedef Tunite* ** TplateauJeu;

TplateauJeu AlloueTab2D(int largeur, int hauteur){
    TplateauJeu jeu;
    jeu = (Tunite***)malloc(sizeof(Tunite**)*largeur);
    for (int i=0;i<largeur;i++){
        jeu[i] = (Tunite**)malloc(sizeof(Tunite*)*hauteur);
    }
    return jeu;  //tab2D contenant des pointeurs
}
void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur){
    for (int i=0;i<largeur;i++){
        for (int j=0;j<hauteur;j++){
            jeu[i][j] = NULL;
        }
    }

    //POUR LA DEMO D'AFFICHAGE UNIQUEMENT, A SUPPRIMER
    //(les tours ici ne sont pas li�es aux listes des unit�s de vos joueurs)
    jeu[5][3]=creeTour(5,3);
    jeu[5][1]=creeTourRoi(5,1);
    jeu[5][15]=creeTour(5,15);
    jeu[5][17]=creeTourRoi(5,17);
    //FIN DEMO AFFICHAGE
}

void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur){
    //pour un affichage sur la console, en relation avec enum TuniteDuJeu
    const char* InitialeUnite[6]={"T", "R", "A", "C", "D", "G"};

    printf("\n");
    for (int j=0;j<hauteur;j++){
        for (int i=0;i<largeur;i++){
                // A ne pas donner aux etudiants
            if (jeu[i][j] != NULL){
                    printf("%s",InitialeUnite[jeu[i][j]->nom]);
            }
            else printf(" ");  //cad pas d'unit� sur cette case
        }
        printf("\n");
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase combat ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool tourRoiDetruite(TListePlayer player){
    TListePlayer courant = player;

    //cherche la cellule Tunite, avec pour pour nom TuniteDuJeu = tourRoi
    while(!listeVide(courant)){

        if ((*(courant->pdata)).nom == tourRoi){
            return ((*(courant->pdata)).pointsDeVie >= 0);
        }
        else{
            courant = courant->suiv;
        }
    }
    //printf("tourRoiDetruite : pas de tour Roi dans la liste");
    return true;
}

void supprimerUnite(TListePlayer *player, Tunite *UniteDetruite, TplateauJeu jeu){

    TListePlayer courant = *player;

    bool find; // condition d'arret pour la boucle

    // variable position dans la liste et positions X/Y de l'untit� d�truite
    int pos_liste = 0, posX = UniteDetruite->posX, posY = UniteDetruite->posY;

    // variable pour les positions X/Y de l'untit� courante
    int current_posX, current_posY;

    while(!listeVide(courant) || find){

        current_posX = get_posX(courant);
        current_posY = get_posY(courant);

        if(posX == current_posX && posY == current_posY){
           // Unite � d�truire trouv�
           // On supprime l'unit� de la liste
           player = suppEnN(*player, pos_liste);

           //on supprime l'unit� du plateau
           free(jeu[posX][posY]);
           jeu[posX][posY] = NULL;
           find = true;
        }
        else{   //
            courant = courant->suiv;
            pos_liste++;
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase combat //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase D�placement ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// connaitre joueur haut ou bas ?
TListePlayer* DeplacementUnite(TListePlayer *player, TplateauJeu jeu){


}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase D�placement /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase creation //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Tunite *creeTour(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = tour;
    nouv->cibleAttaquable = solEtAir;
    nouv->maposition = sol;
    nouv->pointsDeVie = 500;
    nouv->vitesseAttaque = 1.0;
    nouv->degats = 100;
    nouv->portee = 3;
    nouv->vitessedeplacement = 0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 0;
    return nouv;
}
Tunite *creeTourRoi(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = tourRoi;
    nouv->cibleAttaquable = solEtAir;
    nouv->maposition = sol;
    nouv->pointsDeVie = 800;
    nouv->vitesseAttaque = 1.2;
    nouv->degats = 120;
    nouv->portee = 4;
    nouv->vitessedeplacement = 0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 0;
    return nouv;
}
Tunite *creeArcher(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = archer;
    nouv->cibleAttaquable = solEtAir;
    nouv->maposition = sol;
    nouv->pointsDeVie = 80;
    nouv->vitesseAttaque = 1.1;
    nouv->degats = 120;
    nouv->portee = 3;
    nouv->vitessedeplacement = 1.0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 2;
    return nouv;
}
Tunite *creeGargouille(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = gargouille;
    nouv->cibleAttaquable = solEtAir;
    nouv->maposition = air;
    nouv->pointsDeVie = 80;
    nouv->vitesseAttaque = 0.6;
    nouv->degats = 90;
    nouv->portee = 1;
    nouv->vitessedeplacement = 3.0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 1;
    return nouv;
}
Tunite *creeDragon(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = dragon;
    nouv->cibleAttaquable = solEtAir;
    nouv->maposition = air;
    nouv->pointsDeVie = 200;
    nouv->vitesseAttaque = 1.1;
    nouv->degats = 70;
    nouv->portee = 2;
    nouv->vitessedeplacement = 2.0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 3;
    return nouv;
}
Tunite *creeChevalier(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    nouv->nom = chevalier;
    nouv->cibleAttaquable = sol;
    nouv->maposition = sol;
    nouv->pointsDeVie = 400;
    nouv->vitesseAttaque = 1.5;
    nouv->degats = 250;
    nouv->portee = 1;
    nouv->vitessedeplacement = 2.0;
    nouv->posX = posx;
    nouv->posY = posy;
    nouv->peutAttaquer = 1;
    nouv->coutEnElixir = 4;
    return nouv;
}

Tunite *AcheteUnite(int *elixirEnStockduJoueur){

    if(*elixirEnStockduJoueur <= 0){
        // Le joueur n'a pas d'�lixir et ne peut donc pas acheter d'unite
        return NULL;
    }
    //le joueur a assez d'�lixir pour acheter une unite
    else{
        // valeur al�atoire pour d�cider si l'ia achete ce tour
        int randint;
        randint = rand()%2; // 1 = achat | 0 = pas d'achat

        //l'ia achete ce tour
        if(randint == 1){

            Tunite *nouvelleUnite = (Tunite*)malloc(sizeof(Tunite));

            if(*elixirEnStockduJoueur >= 4){
                randint = rand()%4 +1; // pour choisir une unit�
            }
            else{   // si l'ia n'a pas assez d'elixir pour acheter certaines unit�s
                randint = rand()%(4-(*elixirEnStockduJoueur)) +1;
            }

            switch(randint) {
                case 1:
                    //creation de l'unit� choisit al�atoirement (set X et Y � 0 par d�fault)
                    nouvelleUnite = creeGargouille(0, 0);
                    //on enl�ve de l'elixir � l'ia en fonction de l'unite achet�e
                    break;
                case 2:
                    nouvelleUnite = creeArcher(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 2;
                    break;
                case 3:
                    nouvelleUnite = creeDragon(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 3;
                    break;
                case 4:
                    nouvelleUnite = creeChevalier(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 4;
                    break;
                default:
                    printf("Erreur AcheteUnite : le nombre al�atoire invalide !\n");
                    exit(EXIT_FAILURE);
            }

            return nouvelleUnite;
        }
        else{
            //l'ia n'achete pas ce tour (randint == 0)
            return NULL;
        }


    }
}

Tunite *InitAndFindFreePos_HAUT(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 5;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 2 <= i <= 8 et place libre non trouv�e
    while(i >= 2 && i <= 8 && !find){

        // changement de colonne (Order X : 4->6->3->7->2->8)
        if(side == 1){
            // switch sur le c�t� droit
            change_sideN = change_sideN -2;
            i = i + change_sideN;
            side++;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
            side--;
        }
        // Evolution change_sideN : 1->-1>-3->-5
        // Evolution change_sidep : 0->2->4->6

        while(j >= 2 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(getPtrData(Unite_A_Placer), i); // on change les coord
                setPosXUnite(getPtrData(Unite_A_Placer), j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j--; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

Tunite *InitAndFindFreePos_BAS(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 13;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 2 <= i <= 8 et place libre non trouv�e
    while(i >= 2 && i <= 8 && !find){

        // changement de colonne (Order X (i) : 4->6->3->7->2->8)
        if(side == 1){
            // switch sur le c�t� droite
            change_sideN = change_sideN -2;
            i = i + change_sideN;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
        }
        // Evolution change_sideN : 1->-1>-3->-5
        // Evolution change_sidep : 0->2->4->6

        while(j <= 16 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(getPtrData(Unite_A_Placer), i); // on change les coord
                setPosXUnite(getPtrData(Unite_A_Placer), j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j++; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}


// On ne renvoit rien et on ajoute en t�te pour la complexit�
//--> le pointeur donn� ne sera plus le 1er donc utilis� getptrFirstCell()
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite){
    TListePlayer courant = *player;

    // on ajoute la nouvelle unit� que si la liste n'est pas vide (fin de partie)
    if(!listeVide(courant)){
        ajoutEnTete(courant, *nouvelleUnite);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase creation ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase elixir ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int elixirGiven(){
    int randint;
    randint = rand()%3 + 1;
    return randint;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase elixir //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




