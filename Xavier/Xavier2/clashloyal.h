#ifndef JEU2048_H_INCLUDED
#define JEU2048_H_INCLUDED

#define LARGEURJEU 11
#define HAUTEURJEU 19

#include "types.h"
#include "listeDouble.h"

TplateauJeu AlloueTab2D(int largeur, int hauteur);
void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur);
void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur);



bool tourRoiDetruite(TListePlayer player);
void supprimerUnite(TListePlayer *player, Tunite *UniteDetruite, TplateauJeu jeu);

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase D�placement ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

//renvoie les nouvelles liste des joueurs avec les changement de pos des unites
TListePlayer* DeplacementUnite(TListePlayer *player, TplateauJeu jeu);

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// Phase creation ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

Tunite *creeArcher(int posx, int posy);
Tunite *creeGargouille(int posx, int posy);
Tunite *creeDragon(int posx, int posy);
Tunite *creeChevalier(int posx, int posy);
Tunite *creeTour(int posx, int posy);
Tunite *creeTourRoi(int posx, int posy);

Tunite *AcheteUnite(int *elixirEnStockduJoueur);
Tunite *InitAndFindFreePos_HAUT(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu);
Tunite *InitAndFindFreePos_BAS(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu);
// random entre un c�t� ou l'autre ?
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite);


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// Phase elixir //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int elixirGiven();



/*
void PositionnePlayerOnPlateau(TListePlayer player, TplateauJeu jeu)

TListePlayer quiEstAPortee(TListePlayer player, Tunite *uneUniteDeLautreJoueur);
TListePlayer combat(TListePlayer player, Tunite *uneUniteDeLautreJoueur);





*/

#endif // JEU2048_H_INCLUDED
