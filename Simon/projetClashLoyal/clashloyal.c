#include "SDL.h"
#include "clashloyal.h"
#include <stdio.h>
#include <stdlib.h>


TplateauJeu AlloueTab2D(int largeur, int hauteur){
    TplateauJeu jeu;
    jeu = (Tunite***)malloc(sizeof(Tunite**)*largeur);
    for (int i=0;i<largeur;i++){
        jeu[i] = (Tunite**)malloc(sizeof(Tunite*)*hauteur);
    }
    return jeu;  //tab2D contenant des pointeurs
}

void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur){
    for (int i=0;i<largeur;i++){
        for (int j=0;j<hauteur;j++){
            jeu[i][j] = NULL;
        }
    }
}

void PositionnePlayerOnPlateau(TListePlayer player, TplateauJeu jeu){
    TListePlayer courant = player;
    int x, y;
    while (!listeVide(courant)){
        x = getPosXUnite(getPtrData(courant));
        y = getPosYUnite(getPtrData(courant));
        jeu[x][y] = getPtrData(courant);
        courant = getptrNextCell(courant);
    }
}

void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur){
    //pour un affichage sur la console, en relation avec enum TuniteDuJeu
    const char* InitialeUnite[6]={"T", "R", "A", "C", "D", "G"};

    printf("\n");
    for (int j=0;j<hauteur;j++){
        for (int i=0;i<largeur;i++){
                // A ne pas donner aux etudiants
            if (jeu[i][j] != NULL){
                    printf("%s",InitialeUnite[jeu[i][j]->nom]);
            }
            else printf(" ");  //cad pas d'unit� sur cette case
        }
        printf("\n");
    }
}

//Comparateurs

bool aPlusDePV(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a plus de vie que UniteB
    return (getPointsDeVieUnite(UniteA) > getPointsDeVieUnite(UniteB));
}

bool attaquePlusVite(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA attaque avant UniteB
    return (getVitesseAttaqueUnite(UniteA) < getVitesseAttaqueUnite(UniteB));
}

bool faitPlusDeDmg(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA fait plus de degats que UniteB
    return (getDegatsUnite(UniteA) > getDegatsUnite(UniteB));
}

bool aPlusDePortee(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a plus de portee que UniteB
    return (getPorteeUnite(UniteA) > getPorteeUnite(UniteB));
}

bool coutePlusCher(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA coute plus cher que UniteB
    return (getCoutEnElixirUnite(UniteA) > getCoutEnElixirUnite(UniteB));
}


bool aMoinsDePV(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a moins de vie que UniteB
    return (getPointsDeVieUnite(UniteA) < getPointsDeVieUnite(UniteB));
}

bool attaqueMoinsVite(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA attaque apres UniteB
    return (getVitesseAttaqueUnite(UniteA) > getVitesseAttaqueUnite(UniteB));
}

bool faitMoinsDeDmg(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA fait moins de degats que UniteB
    return (getDegatsUnite(UniteA) < getDegatsUnite(UniteB));
}

bool aMoinsDePortee(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a moins de portee que UniteB
    return (getPorteeUnite(UniteA) < getPorteeUnite(UniteB));
}

bool couteMoinsCher(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA coute moins cher que UniteB
    return (getCoutEnElixirUnite(UniteA) < getCoutEnElixirUnite(UniteB));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase combat ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setPeutAttaquerListe(TListePlayer player, canAttack val){
    TListePlayer courant = player;
    while (!listeVide(courant)){
        setPeutAttaquerUnite(getPtrData(courant), val);
        courant = getptrNextCell(courant);
    }
}

int distEntreUnite(Tunite* unite1, Tunite* unite2){
    int distX = getPosXUnite(unite1) - getPosXUnite(unite2);
    if (distX < 0)  //valeur absolue
        distX = -distX;
    int distY = getPosYUnite(unite1) - getPosYUnite(unite2);
    if (distY < 0)  //valeur absolue
        distY = -distY;
    return distX + distY;
}

bool peutAttaquerCible(Tunite* uniteAtk, Tunite* uniteDef){
    int range = getPorteeUnite(uniteAtk);
    Tcible cibleUniteAtk = getCibleAttaquableUnite(uniteAtk);
    Tcible positionUniteDef = getMaPositionUnite(uniteDef);
    bool positionAttaquable = false;
    if (cibleUniteAtk == solEtAir){
        positionAttaquable = true;
    } else {
        positionAttaquable = (cibleUniteAtk == positionUniteDef);
    }
    return (distEntreUnite(uniteAtk, uniteDef) <= range) && positionAttaquable;
}

TListePlayer estAPortee(Tunite * uniteAtk, TListePlayer playerDef, TplateauJeu jeu){
    TListePlayer listeCourant = playerDef;
    TListePlayer aPortee;
    initListe(&aPortee);
    Tunite * uniteCourant;
    while (!listeVide(listeCourant)){
        uniteCourant = getPtrData(listeCourant);
        if (peutAttaquerCible(uniteAtk, uniteCourant)){
            aPortee = ajoutEnTetePtr(aPortee, uniteCourant);
        }
        listeCourant = getptrNextCell(listeCourant);
    }
    tri_selection_liste(aPortee, aMoinsDePV);
    return aPortee;
}

bool unitePeutAttaquer(Tunite* unite){
    return (getPeutAttaquerUnite(unite)==1);
}

bool uniteEstEnVie(Tunite* unite){
    return (getPointsDeVieUnite(unite)>0);
}

void attaque(Tunite* attaquant, TListePlayer peutAttaquerListe){
    TListePlayer courant = peutAttaquerListe;
    Tunite* recoitAtk;
    while (unitePeutAttaquer(attaquant) && uniteEstEnVie(attaquant) && !listeVide(courant)){
        recoitAtk = getPtrData(courant);
        if (uniteEstEnVie(recoitAtk)){
            setPointsDeVieUnite(recoitAtk, getPointsDeVieUnite(recoitAtk) - getDegatsUnite(attaquant));
            setPeutAttaquerUnite(attaquant, 0);
        } else {
            courant = getptrNextCell(courant);
        }
    }
}

void combat(TListePlayer player1, TListePlayer player2, TplateauJeu jeu){
    TListePlayer courantP1 = player1, courantP2 = player2;
    TListePlayer aPortee;
    Tunite* uniteAtk;
    initListe(&aPortee);
    while (!listeVide(courantP1) || !listeVide(courantP2)){
        if (!listeVide(courantP1) && !listeVide(courantP2)){
            if (attaquePlusVite(getPtrData(courantP1), getPtrData(courantP2))){
                uniteAtk = getPtrData(courantP1);
                aPortee = estAPortee(uniteAtk, player2, jeu);
                courantP1 = getptrNextCell(courantP1);
            } else {
                uniteAtk = getPtrData(courantP2);
                aPortee = estAPortee(uniteAtk, player1, jeu);
                courantP2 = getptrNextCell(courantP2);
            } //fin if else
        } else {
            if (!listeVide(courantP1)){ //si courantP1 n'est pas vide, courantP2 l'est
                uniteAtk = getPtrData(courantP1);
                aPortee = estAPortee(uniteAtk, player2, jeu);
                courantP1 = getptrNextCell(courantP1);
            } else { //si courantP1 est vide, courantP2 ne l'est pas
                uniteAtk = getPtrData(courantP2);
                aPortee = estAPortee(uniteAtk, player1, jeu);
                courantP2 = getptrNextCell(courantP2);
            } //fin if else
        } //fin if else
        attaque(uniteAtk, aPortee);
    }  //fin while
}

TListePlayer supprUniteMortes(TListePlayer playerList, TplateauJeu jeu){
    TListePlayer courant = playerList;
    TListePlayer listeFinale = playerList;
    while (!listeVide(courant)){
        if (!uniteEstEnVie(getPtrData(courant))){
            if (listeFinale == courant){
                listeFinale = getptrNextCell(listeFinale);
            }
            jeu[getPosXUnite(getPtrData(courant))][getPosYUnite(getPtrData(courant))] = NULL;
            courant = suppCaseListe(courant);
        } else {
            courant = getptrNextCell(courant);
        }
    }
    return listeFinale;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase combat //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase creation //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Tunite *creeTour(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tour);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 500);
    setVitesseAttaqueUnite(nouv, 1.0);
    setDegatsUnite(nouv, 100);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeTourRoi(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tourRoi);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 800);
    setVitesseAttaqueUnite(nouv, 1.2);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 4);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeArcher(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, archer);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.7);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 1);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeChevalier(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, chevalier);
    setCibleAttaquableUnite(nouv, sol);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 400);
    setVitesseAttaqueUnite(nouv, 1.5);
    setDegatsUnite(nouv, 250);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeDragon(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, dragon);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 200);
    setVitesseAttaqueUnite(nouv, 1.1);
    setDegatsUnite(nouv, 70);
    setPorteeUnite(nouv, 2);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeGargouille(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, gargouille);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.6);
    setDegatsUnite(nouv, 90);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 3);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *AcheteUnite(int *elixirEnStockduJoueur){

    if(*elixirEnStockduJoueur <= 0){
        // Le joueur n'a pas d'�lixir et ne peut donc pas acheter d'unite
        return NULL;
    }
    //le joueur a assez d'�lixir pour acheter une unite
    else{
        // valeur al�atoire pour d�cider si l'ia achete ce tour
        int randint;
        randint = rand()%2; // 1 = achat | 0 = pas d'achat

        //l'ia achete ce tour
        if(randint == 1){

            Tunite *nouvelleUnite = (Tunite*)malloc(sizeof(Tunite));

            if(*elixirEnStockduJoueur >= 4){
                randint = rand()%4 +1; // pour choisir une unit�
            }
            else{   // si l'ia n'a pas assez d'elixir pour acheter certaines unit�s
                randint = rand()%(4-(*elixirEnStockduJoueur)) +1;
            }

            switch(randint) {
                case 1:
                    //creation de l'unit� choisit al�atoirement
                    nouvelleUnite = creeGargouille(0, 0);
                    //on enl�ve de l'elixir � l'ia en fonction de l'unite achet�e
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 1;
                    break;
                case 2:
                    nouvelleUnite = creeArcher(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 2;
                    break;
                case 3:
                    nouvelleUnite = creeDragon(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 3;
                    break;
                case 4:
                    nouvelleUnite = creeChevalier(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 4;
                    break;
                default:
                    printf("Erreur AcheteUnite : le nombre al�atoire invalide !\n");
                    exit(EXIT_FAILURE);
            }

            return nouvelleUnite;
        }
        else{
            //l'ia n'achete pas ce tour (randint == 0)
            return NULL;
        }


    }
}

Tunite *InitAndFindFreePos(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    //diff�rencier joueur 1 et joueur 2 ?
    int i = 1, j = 2;
    bool stop_find = true;
    // X - colones - largeur (0-11)
    while(i <= 8 || stop_find){
        //pas d'unit� au centre de la map
        if(i != 5){

            i++; // on passe � la prochaine colonne

            // Y - ligne - hauteur (0-19)
            while(i <= 5 && stop_find){
                if(jeu[i][j] == NULL){
                    //case vide
                    setPosXUnite(Unite_A_Placer, i); // on change les coord
                    setPosXUnite(Unite_A_Placer, j); // pour ceux de la case vide
                    stop_find = false;
                }
                else{ //case remplie
                   j++; // changement de ligne (X du tableauJeu)
                }
            }
        }
    }

    // on a pas trouv� de place vide
    if(!stop_find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

// On ne renvoit rien et on ajoute en t�te
//--> le pointeur donn� ne sera plus le 1er donc utilis� getptrFirstCell() ?
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite){
    TListePlayer courant = *player;

    // on ajoute la nouvelle unit� que si la liste n'est pas vide (fin de partie)
    if(!listeVide(courant)){
        ajoutEnTete(courant, *nouvelleUnite);
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase creation ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase elixir ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int elixirGiven(){
    int randint;
    randint = rand()%3 + 1;
    return randint;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase elixir //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
