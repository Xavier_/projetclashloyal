#ifndef LISTEDOUBLE_H_INCLUDED
#define LISTEDOUBLE_H_INCLUDED

#include <stdbool.h>
#include "types.h"


void initListe(T_liste *l);
bool listeVide( T_liste l);

T_liste ajoutEnTete(T_liste l, Tunite mydata); //Bien mettre � NULL les champs suiv et prec non utilis�s s'il y en a
T_liste ajoutEnFin(T_liste l, Tunite mydata);
T_liste ajoutEnN(T_liste l, int pos, Tunite mydata);

T_liste suppEnTete(T_liste l);
T_liste suppEnFin(T_liste l);
T_liste suppEnN(T_liste l, int pos);

T_liste getptrFirstCell(T_liste l);
T_liste getptrLastCell(T_liste l);
T_liste getptrNextCell(T_liste l);
T_liste getptrPrevCell(T_liste l);

Tunite* getPtrData(T_liste l);
void swapPtrData( T_liste source, T_liste destination );

int getNbreCell(T_liste l);
int getSizeBytes(T_liste l); //utilisation de sizeof

T_liste creatNewListeFromFusion(T_liste l1, T_liste l2); //on souhaite CREER une nouvelle liste sans modifier l1 et l2
T_liste addBehind(T_liste debut, T_liste suite);

T_liste findCell(T_liste l, Tunite* data);
bool isInList(T_liste l, Tunite* data);
int getOccurences(T_liste l, Tunite data, bool (*comp)(Tunite a, Tunite b));  //nbre de fois que data est pr�sent dans toute la liste l1

void tri_selection_liste(T_liste l, bool (*comp)(Tunite* a, Tunite* b));

T_liste ajoutEnTetePtr(T_liste l, Tunite* mydata);

T_liste suppCaseListe(T_liste l);

//Getters Unite
TuniteDuJeu getNomUnite(Tunite* unite);
Tcible getCibleAttaquableUnite(Tunite* unite);
Tcible getMaPositionUnite(Tunite* unite);
HP getPointsDeVieUnite(Tunite* unite);
atkSpeed getVitesseAttaqueUnite(Tunite* unite);
dmg getDegatsUnite(Tunite* unite);
range getPorteeUnite(Tunite* unite);
moveSpeed getVitesseDeplacementUnite(Tunite* unite);
int getPosXUnite(Tunite* unite);
int getPosYUnite(Tunite* unite);
canAttack getPeutAttaquerUnite(Tunite* unite);
elixirCost getCoutEnElixirUnite(Tunite* unite);

//Setters Unite
void setNomUnite(Tunite* unite, TuniteDuJeu data);
void setCibleAttaquableUnite(Tunite* unite, Tcible data);
void setMaPositionUnite(Tunite* unite, Tcible data);
void setPointsDeVieUnite(Tunite* unite, HP data);
void setVitesseAttaqueUnite(Tunite* unite, atkSpeed data);
void setDegatsUnite(Tunite* unite, dmg data);
void setPorteeUnite(Tunite* unite, range data);
void setVitesseDeplacementUnite(Tunite* unite, moveSpeed data);
void setPosXUnite(Tunite* unite, int data);
void setPosYUnite(Tunite* unite, int data);
void setPeutAttaquerUnite(Tunite* unite, canAttack data);
void setCoutEnElixirUnite(Tunite* unite, elixirCost data);

void afficheListe( T_liste l);

#endif // LISTEDOUBLE_H_INCLUDED

