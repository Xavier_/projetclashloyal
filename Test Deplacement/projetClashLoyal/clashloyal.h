#ifndef JEU2048_H_INCLUDED
#define JEU2048_H_INCLUDED

#define LARGEURJEU 11
#define HAUTEURJEU 19

#include "listeDouble.h"
#include "types.h"



TplateauJeu AlloueTab2D(int largeur, int hauteur);
void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur);
void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur);

//Comparateurs
bool aPlusDePV(Tunite * UniteA, Tunite * UniteB);
bool attaquePlusVite(Tunite * UniteA, Tunite * UniteB);
bool faitPlusDeDmg(Tunite * UniteA, Tunite * UniteB);
bool aPlusDePortee(Tunite * UniteA, Tunite * UniteB);
bool coutePlusCher(Tunite * UniteA, Tunite * UniteB);


//Deplacement
// renvoie si une unit� peut se d�placer
bool Can_moove(Tunite *unite);
//renvoie qui se d�place : true = unite HAUT | false : unite BAS
//bool Who_moove(Tunite player1_HAUT, Tunite player2_BAS);
void Deplacement_Unite(TListePlayer player1, TListePlayer player2, TplateauJeu jeu);
void Deplacement_Unite_J_HAUT(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef);
void Deplacement_Unite_J_BAS(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef);

bool aUneCible(Tunite* uniteAtk, TListePlayer playerDef);

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// Phase combat //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int distEntreUnite(Tunite* unite1, Tunite* unite2);
TListePlayer estAPortee(Tunite * uniteAtk, TListePlayer playerDef, TplateauJeu jeu);
bool unitePeutAttaquer(Tunite* unite);
bool peutAttaquerCible(Tunite* uniteAtk, Tunite* uniteDef);
bool uniteEstEnVie(Tunite* unite);
void attaque(Tunite* attaquant, TListePlayer peutAttaquerListe);
void combat(TListePlayer player1, TListePlayer player2, TplateauJeu jeu);
TListePlayer supprUniteMortes(TListePlayer playerList, TplateauJeu jeu);

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// Phase creation ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
Tunite *creeTour(int posx, int posy);
Tunite *creeTourRoi(int posx, int posy);
Tunite *creeArcher(int posx, int posy);
Tunite *creeGargouille(int posx, int posy);
Tunite *creeDragon(int posx, int posy);
Tunite *creeChevalier(int posx, int posy);

Tunite *AcheteUnite(int *elixirEnStockduJoueur);
Tunite *InitAndFindFreePos_HAUT(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu);
Tunite *InitAndFindFreePos_BAS(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu);
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Phase elixir //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int elixirGiven();



void PositionnePlayerOnPlateau(TListePlayer player, TplateauJeu jeu);
/*
Bool tourRoiDetruite(TListePlayer player);

TListePlayer quiEstAPortee(TListePlayer player, Tunite *uneUniteDeLautreJoueur);
TListePlayer combat(TListePlayer player, Tunite *uneUniteDeLautreJoueur);


Void supprimerUnite(TListePlayer *player, Tunite *UniteDetruite);
Tunite AcheteUnite(int *elixirEnStockduJoueur);
Void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite);

*/

#endif // JEU2048_H_INCLUDED
