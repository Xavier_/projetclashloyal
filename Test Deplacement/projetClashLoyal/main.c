//NOM pr�nom �tudiant 1 :
//NOM pr�nom �tudiant 2 :

#include "SDL.h"
#include "maSDL.h"    //biblioth�que avec des fonction d'affichage pour le jeu 2048
#include "clashloyal.h"
#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>



/*--------- Main ---------------------*/
int main(int argc, char* argv[])
{
    SDL_Window *pWindow;
    SDL_Init(SDL_INIT_VIDEO);

    pWindow = SDL_CreateWindow(
        "Appuyez sur ECHAP pour quitter, S/C ET D/V les gerer les sauvegardes",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        LARGEURJEU*40,
        HAUTEURJEU*40,
        SDL_WINDOW_SHOWN
    );

    SDL_Surface* pWinSurf = SDL_GetWindowSurface(pWindow);  //le sprite qui couvre tout l'�cran
    SDL_Surface* pSpriteTour = SDL_LoadBMP("./data/Tour.bmp");  //indice 0 dans tabSprite (via l'enum TuniteDuJeu)
    SDL_Surface* pSpriteTourRoi = SDL_LoadBMP("./data/TourRoi.bmp"); //indice 1
    SDL_Surface* pSpriteArcher = SDL_LoadBMP("./data/Archer.bmp"); //indice 2
    SDL_Surface* pSpriteChevalier = SDL_LoadBMP("./data/Chevalier.bmp"); //indice 3
    SDL_Surface* pSpriteDragon = SDL_LoadBMP("./data/Dragon.bmp"); //indice 4
    SDL_Surface* pSpriteGargouille = SDL_LoadBMP("./data/Gargouille.bmp"); //indice 5
    SDL_Surface* pSpriteEau = SDL_LoadBMP("./data/Eau.bmp"); //indice 6  Ne figure pas dans l'enum TuniteDuJeu
    SDL_Surface* pSpriteHerbe = SDL_LoadBMP("./data/Herbe.bmp"); //indice 7 idem
    SDL_Surface* pSpritePont = SDL_LoadBMP("./data/Pont.bmp"); //indice 8 idem
    SDL_Surface* pSpriteTerre = SDL_LoadBMP("./data/Terre.bmp"); //indice 9 idem

    // ASTUCE : on stocke le sprite d'une unit� � l'indice de son nom dans le type enum TuniteDuJeu, dans le tableau TabSprite
    // SAUF pour l'Eau, l''herbe et le pont qui apparaitront en l absence d'unit� (NULL dans le plateau) et en foction de certains indices x,y d�finissant le chemin central
    SDL_Surface* TabSprite[10]={pSpriteTour,pSpriteTourRoi,pSpriteArcher,pSpriteChevalier,pSpriteDragon,pSpriteGargouille,pSpriteEau,pSpriteHerbe,pSpritePont,pSpriteTerre};


    if ( pSpriteTour )  //si le permier sprite a bien �t� charg�, on suppose que les autres aussi
    {
        TplateauJeu jeu = AlloueTab2D(LARGEURJEU,HAUTEURJEU);
        initPlateauAvecNULL(jeu,LARGEURJEU,HAUTEURJEU);
        affichePlateauConsole(jeu,LARGEURJEU,HAUTEURJEU);

        prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
        maj_fenetre(pWindow);

        //A COMMENTER quand vous en aurez assez de cliquer sur ces popups ^^

        //message("Welcome in ClashLoyal","Ceci est un point de depart de votre future interface de votre jeu ClahLoyal");
        //message("et fin","ECHAP->quitter, S/C ET D/V les gerer les sauvegardes");

        /**********************************************************************/
        /*                                                                    */
        /*              DEFINISSEZ/INITIALISER ICI VOS VARIABLES              */
        /*                                                                    */
        TListePlayer player1, player2;
        initListe(&player1);
        initListe(&player2);
        /*player1 = ajoutEnTetePtr(player1, creeTourRoi(5, 17));
        player2 = ajoutEnTetePtr(player2, creeTourRoi(5, 1));
        player1 = ajoutEnTetePtr(player1, creeTour(5, 15));
        player2 = ajoutEnTetePtr(player2, creeTour(5, 3));*/
        player1 = ajoutEnTetePtr(player1, creeGargouille(3, 4));
        player1 = ajoutEnTetePtr(player1, creeChevalier(8, 4));
        player2 = ajoutEnTetePtr(player2, creeGargouille(3, 14));
        player2 = ajoutEnTetePtr(player2, creeArcher(8, 13));/*
        player1 = ajoutEnTetePtr(player1, creeArcher(5, 15));
        player2 = ajoutEnTetePtr(player2, creeChevalier(5, 3));
        player1 = ajoutEnTetePtr(player1, creeChevalier(5, 15));
        player2 = ajoutEnTetePtr(player2, creeGargouille(5, 3));*/
        PositionnePlayerOnPlateau(player1, jeu);
        PositionnePlayerOnPlateau(player2, jeu);
        /*
        //tests de supprUniteMortes sur player1
        setPointsDeVieUnite(getPtrData(player1), 0);
        afficheListe(player1);
        player1 = supprUniteMortes(player1, jeu);
        printf("apres supprUniteMortes : \n \n");
        afficheListe(player1);
        //fin tests supprUniteMortes
        */
/*
        //tests de estAPortee
        player2 = ajoutEnTetePtr(player2, creeArcher(5, 14));
        afficheListe(player2);
        PositionnePlayerOnPlateau(player2, jeu);
        TListePlayer aPortee = estAPortee(getPtrData(player2), player1, jeu);
        printf("aPortee : \n\n");
        afficheListe(aPortee);
        //fin tests estAPortee
*/
        // FIN de vos variables
        /**********************************************************************/

        // boucle principale du jeu
        int cont = 9;
        while ( cont != 0 ){   //VOUS DEVEZ GERER (DETECTER) LA FIN DU JEU -> tourRoiDetruite
                SDL_PumpEvents(); //do events


                /***********************************************************************/
                /*                                                                     */
                /*                                                                     */
                //APPELEZ ICI VOS FONCTIONS QUI FONT EVOLUER LE JEU

                //Phase Combat
                /*
                tri_selection_liste(player1, attaquePlusVite);   // Tri des unite de chaque joueurs
                tri_selection_liste(player2, attaquePlusVite);   // par ordre croissant d'attaque
                //printf("PLAYER 1 :\n\n");
                //afficheListe(player1);
                printf("PLAYER 2 :\n\n");
                afficheListe(player2);
                */

                ////////////  test deplacement /////////////////////////////////
                printf("\n\n-------- DEPLACEMENT ------------ :\n\n");
                Deplacement_Unite(player1, player2, jeu);
                /*printf("\n\nPLAYER 1 :\n\n");
                afficheListe(player1);
                printf("PLAYER 2 :\n\n");
                afficheListe(player2);*/
                cont--;
                printf("\n\n-------- FIN DE LA PHASE DEPLACEMENT ----------------- :\n\n");
                ////////////////////////////

                /*combat(player1, player2, jeu);
                player1 = supprUniteMortes(player1, jeu);   //suppression des unites mortes durant le tour
                player2 = supprUniteMortes(player2, jeu);
                setPeutAttaquerListe(player1, 1);   //remise a 1 (oui) de la stat peutAttaquer
                setPeutAttaquerListe(player2, 1);
                */
                //Fin Phase Combat
                /*                                                                     */
                /*                                                                     */
                // FIN DE VOS APPELS
                /***********************************************************************/
                //affichage du jeu � chaque tour
                efface_fenetre(pWinSurf);
                prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
                maj_fenetre(pWindow);
                SDL_Delay(150);  //valeur du d�lai � modifier �ventuellement

                //LECTURE DE CERTAINES TOUCHES POUR LANCER LES RESTAURATIONS ET SAUVEGARDES
                const Uint8* pKeyStates = SDL_GetKeyboardState(NULL);
                if ( pKeyStates[SDL_SCANCODE_V] ){
                        /* Ajouter vos appels de fonctions ci-dessous qd le joueur appuye sur D */

                        // APPELEZ ICI VOTRE FONCTION DE SAUVEGARDE/RESTAURATION DEMANDEE
                        message("Sauvegarde","Placer ici votre fonction de restauration/sauvegarde");

                        //Ne pas modifiez les 4 lignes ci-dessous
                        efface_fenetre(pWinSurf);
                        prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
                        maj_fenetre(pWindow);
                        SDL_Delay(300);
                }
                if ( pKeyStates[SDL_SCANCODE_C] ){
                        /* Ajouter vos appels de fonctions ci-dessous qd le joueur appuye sur C */

                        // APPELEZ ICI VOTRE FONCTION DE SAUVEGARDE/RESTAURATION DEMANDEE
                        message("Sauvegarde","Placer ici votre fonction de restauration/sauvegarde");

                        //Ne pas modifiez les 4 lignes ci-dessous
                        efface_fenetre(pWinSurf);
                        prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
                        maj_fenetre(pWindow);
                        SDL_Delay(300);
                }
                if ( pKeyStates[SDL_SCANCODE_D] ){
                        /* Ajouter vos appels de fonctions ci-dessous qd le joueur appuye sur D */

                        // APPELEZ ICI VOTRE FONCTION DE SAUVEGARDE/RESTAURATION DEMANDEE
                        message("Sauvegarde","Placer ici votre fonction de restauration/sauvegarde");

                        //Ne pas modifiez les 4 lignes ci-dessous
                        efface_fenetre(pWinSurf);
                        prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
                        maj_fenetre(pWindow);
                        SDL_Delay(300);
                }
                if ( pKeyStates[SDL_SCANCODE_S] ){
                        /* Ajouter vos appels de fonctions ci-dessous qd le joueur appyue sur S */

                        // APPELEZ ICI VOTRE FONCTION DE SAUVEGARDE/RESTAURATION DEMANDEE
                        message("Sauvegarde","Placer ici votre fonction de restauration/sauvegarde");

                        //Ne pas modifiez les 4 lignes ci-dessous
                        efface_fenetre(pWinSurf);
                        prepareAllSpriteDuJeu(jeu,LARGEURJEU,HAUTEURJEU,TabSprite,pWinSurf);
                        maj_fenetre(pWindow);
                        SDL_Delay(300);
                }
                if ( pKeyStates[SDL_SCANCODE_ESCAPE] ){
                        cont = 0;  //sortie de la boucle
                }

        }
        //fin boucle du jeu

        SDL_FreeSurface(pSpriteTour); // Lib�ration de la ressource occup�e par le sprite
        SDL_FreeSurface(pSpriteTourRoi);
        SDL_FreeSurface(pSpriteArcher);
        SDL_FreeSurface(pSpriteChevalier);
        SDL_FreeSurface(pSpriteDragon);
        SDL_FreeSurface(pSpriteGargouille);
        SDL_FreeSurface(pSpriteEau);
        SDL_FreeSurface(pSpriteHerbe);
        SDL_FreeSurface(pSpritePont);
        SDL_FreeSurface(pWinSurf);
    }
    else
    {
        fprintf(stdout,"�chec de chargement du sprite (%s)\n",SDL_GetError());
    }

    SDL_DestroyWindow(pWindow);
    SDL_Quit();
    return 0;
}
