#include <stdio.h>
#include <stdlib.h>

#include "listeDouble.h"


//initListe ne fait pas de malloc, juste une initialisation � NULL du pointeur de liste
void initListe(T_liste *l){
*l=NULL;
}


bool listeVide(T_liste l){
    return (l==NULL);
}


T_liste ajoutEnTete(T_liste l, Tunite mydata){
    T_liste nouv = (T_liste)malloc(sizeof(T_cellule));
    nouv->pdata = (Tunite *)malloc(sizeof(Tunite));
    *(nouv->pdata) = mydata;
    nouv->prec = NULL;
    if (listeVide(l)){
        nouv->suiv = NULL;
    } else {
        nouv->suiv = l;
        l->prec = nouv;
    }
    return nouv;
}

T_liste ajoutEnTetePtr(T_liste l, Tunite* mydata){  //ajoute une unite deja existante, sans faire de malloc donc
    T_liste nouv = (T_liste)malloc(sizeof(T_cellule));
    nouv->pdata = mydata;
    nouv->prec = NULL;
    if (listeVide(l)){
        nouv->suiv = NULL;
    } else {
        nouv->suiv = l;
        l->prec = nouv;
    }
    return nouv;
}


T_liste ajoutEnFin(T_liste l, Tunite mydata){
    T_liste nouv = (T_liste)malloc(sizeof(T_cellule));
    nouv->pdata = (Tunite *)malloc(sizeof(Tunite));
    *(nouv->pdata) = mydata;
    nouv->suiv = NULL;
    if (listeVide(l)){
        nouv->prec = NULL;
        return nouv;
    } else {
        T_liste courant = l;
        while (courant->suiv != NULL){
            courant = courant->suiv;
        }
        courant->suiv = nouv;
        nouv->prec = courant;
        return l;
    }
}

T_liste ajoutEnN(T_liste l, int pos, Tunite mydata){
    T_liste nouv = (T_liste)malloc(sizeof(T_cellule));
    nouv->pdata = (Tunite *)malloc(sizeof(Tunite));
    *(nouv->pdata) = mydata;
    nouv->suiv = NULL;
    if (listeVide(l)){
        nouv->prec = NULL;
        return nouv;
    } else {
        if (pos == 0){
            return ajoutEnTete(l, mydata);
        } else {
            T_liste courant = l;
            T_liste courant_suiv;
            courant_suiv =  courant->suiv;
            int i;
            for (i = 1; i<=pos; i++){
                if (courant_suiv == NULL){
                    if (i == pos){
                        nouv->suiv = NULL;
                        nouv->prec = courant;
                        courant->suiv = nouv;
                        return l;
                    } else {
                        printf("Erreur ajoutEnN : la liste n'est pas assez grande\n");
                        return l;
                    }
                } else {
                    courant = courant->suiv;
                    courant_suiv = courant_suiv->suiv;
                }
            }
            nouv->suiv = courant->suiv;
            nouv->prec = courant;
            courant->suiv = nouv;
            courant_suiv->prec = nouv;
            return l;
        }
    }
}


T_liste suppEnTete(T_liste l){
    if (listeVide(l)){
        printf("Erreur suppEnTete : la liste est vide\n");
        return l;
    } else {
        T_liste head = l->suiv;
        if (listeVide(head)){
            free(l->pdata);
            free(l);
            return NULL;
        } else {
            free(l->pdata);
            free(l);
            head->prec = NULL;
            return head;
        }
    }
}


T_liste suppEnFin(T_liste l){
    if (listeVide(l)){
        printf("Erreur suppEnFin : la liste est vide\n");
        return l;
    } else {
        T_liste tail = l;
        T_liste tail_suiv = l->suiv;
        if (listeVide(tail_suiv)){
            free(l->pdata);
            free(l);
            return NULL;
        } else {
            while (tail_suiv->suiv != NULL){
            tail = tail->suiv;
            tail_suiv = tail_suiv->suiv;
            }
            free((tail->suiv)->pdata);
            free(tail->suiv);
            tail->suiv = NULL;
            return l;
        }
    }
}


T_liste suppEnN(T_liste l, int pos){
    if (listeVide(l)){
        printf("Erreur suppEnN : la liste est vide\n");
        return l;
    } else {
        if (pos == 0){
            return suppEnTete(l);
        } else {
            T_liste courant = l;
            int i;
            for (i = 1; i<=pos; i++){
                if (courant->suiv == NULL){
                    printf("Erreur suppEnN : la liste n'est pas assez grande\n");
                    return l;
                } else {
                    courant = courant->suiv;
                }
            }
            (courant->prec)->suiv = courant->suiv;
            if (courant->suiv != NULL)
            (courant->suiv)->prec = courant->prec;
            free(courant->pdata);
            free(courant);
            return l;
        }
    }
}


T_liste getptrFirstCell(T_liste l){
    if (listeVide(l)){
        return l;
    }
    T_liste courant = l;
    while (!listeVide(courant->prec)){
        courant = courant->prec;
    }
    return courant;
}


T_liste getptrLastCell(T_liste l){
    if (listeVide(l)){
        return l;
    }
    T_liste courant = l;
    while (!listeVide(courant->suiv)){
        courant = courant->suiv;
    }
    return courant;
}


T_liste getptrNextCell(T_liste l){
    if (listeVide(l)){
        return l;
    }
    return l->suiv;
}


T_liste getptrPrevCell(T_liste l){
    if (listeVide(l)){
        return l;
    }
    return l->prec;
}


Tunite* getPtrData(T_liste l){
    if (listeVide(l)){
        return NULL;
    }
    return l->pdata;
}


void swapPtrData(T_liste source, T_liste destination){
    if (listeVide(source) || listeVide(destination)){
        printf("Erreur swapPtrData : les liste ne peuvent etre vides\n");
    } else {
        Tunite * tmp;
        tmp = source->pdata;
        source->pdata = destination->pdata;
        destination->pdata = tmp;
    }
}


int getNbreCell(T_liste l){
    if (listeVide(l)) return 0;
    int i = 1;
    T_liste count = l;
    while (!listeVide(count->suiv)){
        count = count->suiv;
        i++;
    }
    return i;
}


int getSizeBytes(T_liste l){
    if (listeVide(l)) return 0;
    int size;
    size = sizeof(T_liste) * getNbreCell(l);
    return size;
}

T_liste createNewListFromFusion(T_liste l1, T_liste l2){
    T_liste nouv = (T_liste)malloc(sizeof(T_cellule));
    nouv->pdata = (Tunite *)malloc(sizeof(Tunite));
    nouv->prec = NULL;
    nouv->suiv = NULL;
    T_liste courant_old, courant_new;
    courant_new = nouv;
    if (!listeVide(l1)){
        courant_old = l1;
        *(courant_new->pdata) = *(courant_old->pdata);
        while (courant_old->suiv != NULL){
            courant_old = courant_old->suiv;
            courant_new->suiv = (T_liste)malloc(sizeof(T_cellule));
            (courant_new->suiv)->prec = courant_new;
            courant_new = courant_new->suiv;
            courant_new->pdata = (Tunite *)malloc(sizeof(Tunite));
            *(courant_new->pdata) = *(courant_old->pdata);
        }
    }
    if (!listeVide(l2)){
        courant_old = l2;
        if (courant_new != nouv){
            courant_new->suiv = (T_liste)malloc(sizeof(T_cellule));
            (courant_new->suiv)->prec = courant_new;
            courant_new = courant_new->suiv;
            courant_new->pdata = (Tunite *)malloc(sizeof(Tunite));
        }
        *(courant_new->pdata) = *(courant_old->pdata);
        while (courant_old->suiv != NULL){
            courant_old = courant_old->suiv;
            courant_new->suiv = (T_liste)malloc(sizeof(T_cellule));
            (courant_new->suiv)->prec = courant_new;
            courant_new = courant_new->suiv;
            courant_new->pdata = (Tunite *)malloc(sizeof(Tunite));
            *(courant_new->pdata) = *(courant_old->pdata);
        }
    }
    courant_new->suiv = NULL;
    return nouv;
}


T_liste addBehind(T_liste debut, T_liste suite){
    if (listeVide(debut)) return suite;
    if (listeVide(suite)) return debut;
    T_liste last = getptrLastCell(debut);
    last->suiv = suite;
    suite->prec = last;
    return debut;
}


T_liste findCell(T_liste l, Tunite* data){
    T_liste courant = l;
    int i, size = getNbreCell(l);
    for (i = 0; i<size; i++){
        if (courant->pdata == data) return courant;
        courant = courant->suiv;
    }
    return courant;
}

bool isInList(T_liste l, Tunite* data){
    T_liste courant = l;
    int i, size = getNbreCell(l);
    for (i = 0; i<size; i++){
        if (courant->pdata == data) return true;
        courant = courant->suiv;
    }
    return false;
}


int getOccurences(T_liste l, Tunite data, bool (*comp)(Tunite a, Tunite b)){
    T_liste courant = l;
    int i, n=0, size = getNbreCell(l);
    for (i = 0; i<size; i++){
        if (comp(data, *(courant->pdata)))
            n++;
        courant = courant->suiv;
    }
    return n;
}


void tri_selection_liste(T_liste l, bool (*comp)(Tunite* a, Tunite* b)){
    Tunite* data;
    T_liste start, curr, cell_min;
    start = l;
    curr = l;
    cell_min = l;
    while (!listeVide(start)){
        data = getPtrData(start);
        curr = getptrNextCell(start);
        cell_min = start;
        while (!listeVide(curr)){
            if ( !(comp(data, getPtrData(curr))) ){
                data = getPtrData(curr);
                cell_min = getptrPrevCell(curr);
                cell_min = getptrNextCell(cell_min);
            }
            curr = getptrNextCell(curr);
        }
        swapPtrData(start, cell_min);
        start = getptrNextCell(start);
    }
}


T_liste suppCaseListe(T_liste l){
    T_liste prev_cell = getptrPrevCell(l);
    T_liste next_cell = getptrNextCell(l);
    if (!listeVide(prev_cell)){
        prev_cell->suiv = next_cell;
    }
    if (!listeVide(next_cell)){
        next_cell->prec = prev_cell;
    }
    free(l->pdata);
    free(l);
    return next_cell;
}

//Getters
TuniteDuJeu getNomUnite(Tunite* unite){
    return unite->nom;
}
Tcible getCibleAttaquableUnite(Tunite* unite){
    return unite->cibleAttaquable;
}
Tcible getMaPositionUnite(Tunite* unite){
    return unite->maposition;
}
HP getPointsDeVieUnite(Tunite* unite){
    return unite->pointsDeVie;
}
atkSpeed getVitesseAttaqueUnite(Tunite* unite){
    return unite->vitesseAttaque;
}
dmg getDegatsUnite(Tunite* unite){
    return unite->degats;
}
range getPorteeUnite(Tunite* unite){
    return unite->portee;
}
moveSpeed getVitesseDeplacementUnite(Tunite* unite){
    return unite->vitessedeplacement;
}
int getPosXUnite(Tunite* unite){
    return unite->posX;
}
int getPosYUnite(Tunite* unite){
    return unite->posY;
}
canAttack getPeutAttaquerUnite(Tunite* unite){
    return unite->peutAttaquer;
}
elixirCost getCoutEnElixirUnite(Tunite* unite){
    return unite->coutEnElixir;
}

//Setters
void setNomUnite(Tunite* unite, TuniteDuJeu data){
    unite->nom = data;
}
void setCibleAttaquableUnite(Tunite* unite, Tcible data){
    unite->cibleAttaquable = data;
}
void setMaPositionUnite(Tunite* unite, Tcible data){
    unite->maposition = data;
}
void setPointsDeVieUnite(Tunite* unite, HP data){
    unite->pointsDeVie = data;
}
void setVitesseAttaqueUnite(Tunite* unite, atkSpeed data){
    unite->vitesseAttaque = data;
}
void setDegatsUnite(Tunite* unite, dmg data){
    unite->degats = data;
}
void setPorteeUnite(Tunite* unite, range data){
    unite->portee = data;
}
void setVitesseDeplacementUnite(Tunite* unite, moveSpeed data){
    unite->vitessedeplacement = data;
}
void setPosXUnite(Tunite* unite, int data){
    unite->posX = data;
}
void setPosYUnite(Tunite* unite, int data){
    unite->posY = data;
}
void setPeutAttaquerUnite(Tunite* unite, canAttack data){
    unite->peutAttaquer = data % 2;
}
void setCoutEnElixirUnite(Tunite* unite, elixirCost data){
    unite->coutEnElixir = data;
}


void afficheListe(T_liste l){
    if (listeVide(l)){
        printf("La liste est vide\n");
    } else {
        T_liste courant = l;
        Tunite* unite;
        char* name;
        while (!listeVide(courant)){
            unite = getPtrData(courant);
            switch (getNomUnite(unite)){
                case 0 : name = "tour"; break;
                case 1 : name = "tour du Roi"; break;
                case 2 : name = "archer"; break;
                case 3 : name = "chevalier"; break;
                case 4 : name = "dragon"; break;
                case 5 : name = "gargouille"; break;
            }
            printf("Nom : %s\n", name);
            switch (getCibleAttaquableUnite(unite)){
                case 0 : name = "sol"; break;
                case 1 : name = "sol Et Air"; break;
                case 2 : name = "air"; break;
            }
            printf("Cible Attaquable : %s\n", name);
            switch (getMaPositionUnite(unite)){
                case 0 : name = "sol"; break;
                case 1 : name = "sol Et Air"; break;
                case 2 : name = "air"; break;
            }
            printf("Ma Position : %s\n", name);
            printf("Points de Vie : %d\n", getPointsDeVieUnite(unite));
            printf("Vitesse d'Attaque : %2f\n", getVitesseAttaqueUnite(unite));
            printf("Degats : %d\n", getDegatsUnite(unite));
            printf("Portee : %d\n", getPorteeUnite(unite));
            printf("Vitesse de deplacement : %d\n", getVitesseDeplacementUnite(unite));
            printf("Position (x, y) : (%d, %d)\n", getPosXUnite(unite), getPosYUnite(unite));
            switch (getPeutAttaquerUnite(unite)){
                case 0 : name = "Non"; break;
                case 1 : name = "Oui"; break;
            }
            printf("Peut attaquer : %s (%d)\n", name, getPeutAttaquerUnite(unite));
            printf("Cout en Elixir : %d\n", getCoutEnElixirUnite(unite));
            courant = getptrNextCell(courant);
            printf("\n");
        }
        printf("\n\n");
    }
}



