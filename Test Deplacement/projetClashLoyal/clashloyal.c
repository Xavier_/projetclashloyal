#include "SDL.h"
#include "clashloyal.h"
#include <stdio.h>
#include <stdlib.h>


TplateauJeu AlloueTab2D(int largeur, int hauteur){
    TplateauJeu jeu;
    jeu = (Tunite***)malloc(sizeof(Tunite**)*largeur);
    for (int i=0;i<largeur;i++){
        jeu[i] = (Tunite**)malloc(sizeof(Tunite*)*hauteur);
    }
    return jeu;  //tab2D contenant des pointeurs
}

void initPlateauAvecNULL(TplateauJeu jeu,int largeur, int hauteur){
    for (int i=0;i<largeur;i++){
        for (int j=0;j<hauteur;j++){
            jeu[i][j] = NULL;
        }
    }
}

void PositionnePlayerOnPlateau(TListePlayer player, TplateauJeu jeu){
    TListePlayer courant = player;
    int x, y;
    while (!listeVide(courant)){
        x = getPosXUnite(getPtrData(courant));
        y = getPosYUnite(getPtrData(courant));
        jeu[x][y] = getPtrData(courant);
        courant = getptrNextCell(courant);
    }
}

void affichePlateauConsole(TplateauJeu jeu, int largeur, int hauteur){
    //pour un affichage sur la console, en relation avec enum TuniteDuJeu
    const char* InitialeUnite[6]={"T", "R", "A", "C", "D", "G"};

    printf("\n");
    for (int j=0;j<hauteur;j++){
        for (int i=0;i<largeur;i++){
                // A ne pas donner aux etudiants
            if (jeu[i][j] != NULL){
                    printf("%s",InitialeUnite[jeu[i][j]->nom]);
            }
            else printf(" ");  //cad pas d'unit� sur cette case
        }
        printf("\n");
    }
}

//Comparateurs

bool aPlusDePV(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a plus de vie que UniteB
    return (getPointsDeVieUnite(UniteA) > getPointsDeVieUnite(UniteB));
}

bool attaquePlusVite(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA attaque avant UniteB
    return (getVitesseAttaqueUnite(UniteA) < getVitesseAttaqueUnite(UniteB));
}

bool faitPlusDeDmg(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA fait plus de degats que UniteB
    return (getDegatsUnite(UniteA) > getDegatsUnite(UniteB));
}

bool aPlusDePortee(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a plus de portee que UniteB
    return (getPorteeUnite(UniteA) > getPorteeUnite(UniteB));
}

bool coutePlusCher(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA coute plus cher que UniteB
    return (getCoutEnElixirUnite(UniteA) > getCoutEnElixirUnite(UniteB));
}


bool aMoinsDePV(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a moins de vie que UniteB
    return (getPointsDeVieUnite(UniteA) < getPointsDeVieUnite(UniteB));
}

bool attaqueMoinsVite(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA attaque apres UniteB
    return (getVitesseAttaqueUnite(UniteA) > getVitesseAttaqueUnite(UniteB));
}

bool faitMoinsDeDmg(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA fait moins de degats que UniteB
    return (getDegatsUnite(UniteA) < getDegatsUnite(UniteB));
}

bool aMoinsDePortee(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA a moins de portee que UniteB
    return (getPorteeUnite(UniteA) < getPorteeUnite(UniteB));
}

bool couteMoinsCher(Tunite * UniteA, Tunite * UniteB){  //retourne true si UniteA coute moins cher que UniteB
    return (getCoutEnElixirUnite(UniteA) < getCoutEnElixirUnite(UniteB));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase combat ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setPeutAttaquerListe(TListePlayer player, canAttack val){
    TListePlayer courant = player;
    while (!listeVide(courant)){
        setPeutAttaquerUnite(getPtrData(courant), val);
        courant = getptrNextCell(courant);
    }
}

int distEntreUnite(Tunite* unite1, Tunite* unite2){
    int distX = getPosXUnite(unite1) - getPosXUnite(unite2);
    if (distX < 0)  //valeur absolue
        distX = -distX;
    int distY = getPosYUnite(unite1) - getPosYUnite(unite2);
    if (distY < 0)  //valeur absolue
        distY = -distY;
    return distX + distY;
}

bool peutAttaquerCible(Tunite* uniteAtk, Tunite* uniteDef){
    int range = getPorteeUnite(uniteAtk);
    Tcible cibleUniteAtk = getCibleAttaquableUnite(uniteAtk);
    Tcible positionUniteDef = getMaPositionUnite(uniteDef);
    bool positionAttaquable = false;
    if (cibleUniteAtk == solEtAir){
        positionAttaquable = true;
    } else {
        positionAttaquable = (cibleUniteAtk == positionUniteDef);
    }
    return (distEntreUnite(uniteAtk, uniteDef) <= range) && positionAttaquable;
}

TListePlayer estAPortee(Tunite * uniteAtk, TListePlayer playerDef, TplateauJeu jeu){
    TListePlayer listeCourant = playerDef;
    TListePlayer aPortee;
    initListe(&aPortee);
    Tunite * uniteCourant;
    while (!listeVide(listeCourant)){
        uniteCourant = getPtrData(listeCourant);
        if (peutAttaquerCible(uniteAtk, uniteCourant)){
            aPortee = ajoutEnTetePtr(aPortee, uniteCourant);
        }
        listeCourant = getptrNextCell(listeCourant);
    }
    tri_selection_liste(aPortee, aMoinsDePV);
    return aPortee;
}

bool unitePeutAttaquer(Tunite* unite){
    return (getPeutAttaquerUnite(unite)==1);
}

bool uniteEstEnVie(Tunite* unite){
    return (getPointsDeVieUnite(unite)>0);
}

void attaque(Tunite* attaquant, TListePlayer peutAttaquerListe){
    TListePlayer courant = peutAttaquerListe;
    Tunite* recoitAtk;
    while (unitePeutAttaquer(attaquant) && uniteEstEnVie(attaquant) && !listeVide(courant)){
        recoitAtk = getPtrData(courant);
        if (uniteEstEnVie(recoitAtk)){
            setPointsDeVieUnite(recoitAtk, getPointsDeVieUnite(recoitAtk) - getDegatsUnite(attaquant));
            setPeutAttaquerUnite(attaquant, 0);
        } else {
            courant = getptrNextCell(courant);
        }
    }
}

void combat(TListePlayer player1, TListePlayer player2, TplateauJeu jeu){
    TListePlayer courantP1 = player1, courantP2 = player2;
    TListePlayer aPortee;
    Tunite* uniteAtk;
    initListe(&aPortee);
    while (!listeVide(courantP1) || !listeVide(courantP2)){
        if (!listeVide(courantP1) && !listeVide(courantP2)){
            if (attaquePlusVite(getPtrData(courantP1), getPtrData(courantP2))){
                uniteAtk = getPtrData(courantP1);
                aPortee = estAPortee(uniteAtk, player2, jeu);
                courantP1 = getptrNextCell(courantP1);
            } else {
                uniteAtk = getPtrData(courantP2);
                aPortee = estAPortee(uniteAtk, player1, jeu);
                courantP2 = getptrNextCell(courantP2);
            } //fin if else
        } else {
            if (!listeVide(courantP1)){ //si courantP1 n'est pas vide, courantP2 l'est
                uniteAtk = getPtrData(courantP1);
                aPortee = estAPortee(uniteAtk, player2, jeu);
                courantP1 = getptrNextCell(courantP1);
            } else { //si courantP1 est vide, courantP2 ne l'est pas
                uniteAtk = getPtrData(courantP2);
                aPortee = estAPortee(uniteAtk, player1, jeu);
                courantP2 = getptrNextCell(courantP2);
            } //fin if else
        } //fin if else
        attaque(uniteAtk, aPortee);
    }  //fin while
}

TListePlayer supprUniteMortes(TListePlayer playerList, TplateauJeu jeu){
    TListePlayer courant = playerList;
    TListePlayer listeFinale = playerList;
    while (!listeVide(courant)){
        if (!uniteEstEnVie(getPtrData(courant))){
            if (listeFinale == courant){
                listeFinale = getptrNextCell(listeFinale);
            }
            jeu[getPosXUnite(getPtrData(courant))][getPosYUnite(getPtrData(courant))] = NULL;
            courant = suppCaseListe(courant);
        } else {
            courant = getptrNextCell(courant);
        }
    }
    return listeFinale;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase combat //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase creation //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Tunite *creeTour(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tour);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 500);
    setVitesseAttaqueUnite(nouv, 1.0);
    setDegatsUnite(nouv, 100);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeTourRoi(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, tourRoi);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 800);
    setVitesseAttaqueUnite(nouv, 1.2);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 4);
    setVitesseDeplacementUnite(nouv, 0);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeArcher(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, archer);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.7);
    setDegatsUnite(nouv, 120);
    setPorteeUnite(nouv, 3);
    setVitesseDeplacementUnite(nouv, 1);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeChevalier(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, chevalier);
    setCibleAttaquableUnite(nouv, sol);
    setMaPositionUnite(nouv, sol);
    setPointsDeVieUnite(nouv, 400);
    setVitesseAttaqueUnite(nouv, 1.5);
    setDegatsUnite(nouv, 250);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeDragon(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, dragon);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 200);
    setVitesseAttaqueUnite(nouv, 1.1);
    setDegatsUnite(nouv, 70);
    setPorteeUnite(nouv, 2);
    setVitesseDeplacementUnite(nouv, 2);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *creeGargouille(int posx, int posy){
    Tunite *nouv = (Tunite*)malloc(sizeof(Tunite));
    setNomUnite(nouv, gargouille);
    setCibleAttaquableUnite(nouv, solEtAir);
    setMaPositionUnite(nouv, air);
    setPointsDeVieUnite(nouv, 80);
    setVitesseAttaqueUnite(nouv, 0.6);
    setDegatsUnite(nouv, 90);
    setPorteeUnite(nouv, 1);
    setVitesseDeplacementUnite(nouv, 3);
    setPosXUnite(nouv, posx);
    setPosYUnite(nouv, posy);
    setPeutAttaquerUnite(nouv, 1);
    setCoutEnElixirUnite(nouv, 0);
    return nouv;
}

Tunite *AcheteUnite(int *elixirEnStockduJoueur){

    if(*elixirEnStockduJoueur <= 0){
        // Le joueur n'a pas d'�lixir et ne peut donc pas acheter d'unite
        return NULL;
    }
    //le joueur a assez d'�lixir pour acheter une unite
    else{
        // valeur al�atoire pour d�cider si l'ia achete ce tour
        int randint;
        randint = rand()%2; // 1 = achat | 0 = pas d'achat

        //l'ia achete ce tour
        if(randint == 1){

            Tunite *nouvelleUnite = (Tunite*)malloc(sizeof(Tunite));

            if(*elixirEnStockduJoueur >= 4){
                randint = rand()%4 +1; // pour choisir une unit�
            }
            else{   // si l'ia n'a pas assez d'elixir pour acheter certaines unit�s
                randint = rand()%(4-(*elixirEnStockduJoueur)) +1;
            }

            switch(randint) {
                case 1:
                    //creation de l'unit� choisit al�atoirement
                    nouvelleUnite = creeGargouille(0, 0);
                    //on enl�ve de l'elixir � l'ia en fonction de l'unite achet�e
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 1;
                    break;
                case 2:
                    nouvelleUnite = creeArcher(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 2;
                    break;
                case 3:
                    nouvelleUnite = creeDragon(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 3;
                    break;
                case 4:
                    nouvelleUnite = creeChevalier(0, 0);
                    *elixirEnStockduJoueur = *elixirEnStockduJoueur - 4;
                    break;
                default:
                    printf("Erreur AcheteUnite : le nombre al�atoire invalide !\n");
                    exit(EXIT_FAILURE);
            }

            return nouvelleUnite;
        }
        else{
            //l'ia n'achete pas ce tour (randint == 0)
            return NULL;
        }


    }
}

Tunite *InitAndFindFreePos_HAUT(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 5;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 1 <= i <= 9 et place libre non trouv�e
    while(i >= 1 && i <= 9 && !find){

        // changement de colonne (Order X : 4->6->3->7->2->8->1->9)
        if(side == 1){
            // switch sur le c�t� droit
            change_sideN = change_sideN -2;
            i = i + change_sideN;
            side++;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
            side--;
        }
        // Evolution change_sideN : 1->-1>-3->-5->-7
        // Evolution change_sidep : 0->2->4->6->8 (+)

        while(j >= 3 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(Unite_A_Placer, i); // on change les coord
                setPosXUnite(Unite_A_Placer, j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j--; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

Tunite *InitAndFindFreePos_BAS(TListePlayer *player, Tunite *Unite_A_Placer, TplateauJeu jeu){
    // i = X = colonne (0-11)| j = Y = ligne (0-19)
    int i = 5, j = 13;
    int side = 1, change_sideN = 1, change_sideP = 0;
    bool find = false;

    // boucle tant que 1 <= i <= 9 et place libre non trouv�e
    while(i >= 1 && i <= 9 && !find){

        // changement de colonne (Order X (i) : 4->6->3->7->2->8->1->9)
        if(side == 1){
            // switch sur le c�t� droite
            change_sideN = change_sideN -2;
            i = i + change_sideN;
        }else{
            // switch sur le c�t� gauche
            change_sideP = change_sideP + 2;
            i = i + change_sideP;
        }
        // Evolution change_sideN : 1->-1>-3->-5->-7
        // Evolution change_sidep : 0->2->4->6->8

        while(j <= 15 && !find){
            if(jeu[i][j] == NULL){
                //case vide
                setPosXUnite(Unite_A_Placer, i); // on change les coord
                setPosXUnite(Unite_A_Placer, j); // par ceux de la case vide
                find = true;
            }
            else{ //case remplie
               j++; // prochaine ligne
            }
        }

    }

    // on a pas trouv� de place vide
    if(!find){
        printf("InitAndFindFreePos : Pas de place disponible.");
        //return NULL;
    }

    return Unite_A_Placer;
}

// On ne renvoit rien et on ajoute en t�te
//--> le pointeur donn� ne sera plus le 1er donc utilis� getptrFirstCell() ?
void AjouterUnite(TListePlayer *player, Tunite *nouvelleUnite){
    TListePlayer courant = *player;

    // on ajoute la nouvelle unit� que si la liste n'est pas vide (fin de partie)
    if(!listeVide(courant)){
        ajoutEnTete(courant, *nouvelleUnite);
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// FIN Phase creation ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*--------------------------------------------------------------------------------------------------------------------------*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// DEBUT Phase elixir ////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int elixirGiven(){
    int randint;
    randint = rand()%3 + 1;
    return randint;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase elixir //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// D�termine si l'unit� � de la vitesse de d�placement
//  retourne true si la vitesse de d�placement de cette unit� est sup�rieure � z�ro
bool Can_move(Tunite *unite){
    return (getVitesseDeplacementUnite(unite) > 0);
}

bool aUneCible(Tunite *uniteAtk, TListePlayer playerDef){   //retourne true si l'uniteAtk est a portee d'au moins 1 unite adverse
    TListePlayer listeCourant = playerDef;
    Tunite *uniteCourant;
    while (!listeVide(listeCourant)){
        uniteCourant = getPtrData(listeCourant);
        if (peutAttaquerCible(uniteAtk, uniteCourant)){
            return true;
        } else {
            listeCourant = getptrNextCell(listeCourant);
        }
    }
    return false;
}

int is_Free_deplacement_X(TplateauJeu jeu, int unitePosX, int unitePosY, int deplace){
    if(jeu[unitePosX+deplace][unitePosY] == NULL){
        //la case est libre
        return deplace;
    }else{
        //la case n'est pas libre
        return 0;
    }
}

int is_Free_deplacement_Y(TplateauJeu jeu, int unitePosX, int unitePosY, int deplace){
    if(jeu[unitePosX][unitePosY+deplace] == NULL){
        //la case est libre
        return deplace;
    }else{
        //la case n'est pas libre
        return 0;
    }
}


void Deplacement_Unite_J_HAUT(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef){

        moveSpeed Nbr_Case_deplace; // vitesse deplacement de l'unite
        printf("\n Debut unite J1 - HAUT\n");
        // coordonn�es de l'unite � d�placer
        int unitePosX = getPosXUnite(unite_deplace);
        int startingPosX = unitePosX;
        int unitePosY = getPosYUnite(unite_deplace);
        int startingPosY = unitePosY;

        printf("J1 - HAUT || COMMENCE en X = %d et Y = %d \n\n", unitePosX, unitePosY);
        Nbr_Case_deplace = getVitesseDeplacementUnite(unite_deplace);

        for(int i = 0; i < Nbr_Case_deplace; i++){
            printf("J1 - HAUT || il reste %d case a se deplacer\n", Nbr_Case_deplace-i);

            if(!aUneCible(unite_deplace, playerDef)){
                //il n'y a pas de cible � portee
                if(0 <= unitePosX && unitePosX < 5){
                    //l'unite se trouve du c�t� gauche
                    if((unitePosX+unitePosY)%2 == 0 || (unitePosX%2 !=0 && unitePosY%2 !=0 )){
                        // X+Y pair ou X et Y impair: d�placement = X+1
                        unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, 1);
                    }else{
                        //X-Y = pair/impair ou impair/pair: d�placement = Y+1
                        unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                    }
                }
                else{
                    if(5 < unitePosX && unitePosX <= 10){
                        //l'unite se trouve du c�t� droit
                        if((unitePosX+unitePosY)%2 == 0 || (unitePosX%2 !=0 && unitePosY%2 !=0 )){
                            // X+Y pair ou X et Y impair: d�placement = X-1
                            unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, -1);
                        }else{
                            //X-Y = pair/impair ou impair/pair: d�placement = Y+1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                        }
                    }else{
                        if(unitePosX == 5){
                            //l'unite se trouve au centre : d�placement = Y+1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, 1);
                        }else{
                            printf("Erreur coord X unite hors plateau, val X = %d", unitePosX);
                        }
                    }
                }
            }
            else{//il y a une cible � portee -> Arret boucle/deplacement -> pas de d�placement
                break;
            }
            //on modifie les valeur X et Y de l'unite
            setPosXUnite(unite_deplace, unitePosX); //on change la valeur de X
            setPosYUnite(unite_deplace, unitePosY); //on change la valeur de Y
            printf("   ----  Avancement X = %d et Y = %d \n", unitePosX, unitePosY);
        }//Fin de la boucle

        //on re-v�rifie que la case est bien vide
        if(jeu[unitePosX][unitePosY] == NULL){
            //la case est bien vide

            //printf("Pointeur starting, contenu dans jeu : %p", jeu[startingPosX][startingPosY]);

            // on met � null l'ancien emplacement dans jeu
            jeu[startingPosX][startingPosY] = NULL;
            //on rajoute le pointeur de l'unite dans la nouvelle case
            jeu[unitePosX][unitePosY] = unite_deplace;
        }
        else{
            printf("\n\n ERREUR Deplacement_Unite_J_HAUT - Joueur 1 (ou pas de deplacement)\n\n");
        }
}

void Deplacement_Unite_J_BAS(Tunite *unite_deplace, TplateauJeu jeu, TListePlayer playerDef){
        printf("\n ---- Debut unite J2 - BAS ------\n");
        moveSpeed Nbr_Case_deplace; // vitesse deplacement de l'unite

        // coordonn�es de l'unite � d�placer
        int unitePosX = getPosXUnite(unite_deplace);
        int startingPosX = unitePosX;
        int unitePosY = getPosYUnite(unite_deplace);
        int startingPosY = unitePosY;

        printf("J2 - BAS || COMMENCE en X = %d et Y = %d \n\n", unitePosX, unitePosY);
        Nbr_Case_deplace = getVitesseDeplacementUnite(unite_deplace);

        for(int i = 0; i < Nbr_Case_deplace; i++){
            printf("J2 - BAS il reste %d case a se deplacer\n", Nbr_Case_deplace-i);

            if(!aUneCible(unite_deplace, playerDef)){
                //il n'y a pas de cible � portee
                if(0 <= unitePosX && unitePosX < 5){
                    //l'unite se trouve du c�t� gauche
                    if((unitePosX+unitePosY)%2 == 0 || (unitePosX%2 !=0 && unitePosY%2 !=0 )){
                        // X+Y pair ou X et Y impair: d�placement = X+1
                        unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, 1);
                    }else{
                        //X-Y = pair/impair ou impair/pair: d�placement = Y-1
                        unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                    }
                }
                else{
                    if(5 < unitePosX && unitePosX <= 10){
                        //l'unite se trouve du c�t� droit
                        if((unitePosX+unitePosY)%2 == 0 || (unitePosX%2 !=0 && unitePosY%2 !=0 )){
                            // X+Y pair ou X et Y impair: d�placement = X-1
                            unitePosX += is_Free_deplacement_X(jeu, unitePosX, unitePosY, -1);
                        }else{
                            //X-Y = pair/impair ou impair/pair: d�placement = Y-1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                        }
                    }else{
                        if(unitePosX == 5){
                            //l'unite se trouve au centre : d�placement = Y-1
                            unitePosY += is_Free_deplacement_Y(jeu, unitePosX, unitePosY, -1);
                        }else{
                            printf("Erreur coord X unite hors plateau, val X = %d", unitePosX);
                        }
                    }
                }
            }
            else{//il y a une cible � portee -> Arret boucle/deplacement
                printf("-----> l'unite a une cible !");
                break;
            }
            //on modifie les valeur X et Y de l'unite
            setPosXUnite(unite_deplace, unitePosX); //on change la valeur de X
            setPosYUnite(unite_deplace, unitePosY); //on change la valeur de Y
            printf("J2 - BAS || Avancement X = %d et Y = %d \n", unitePosX, unitePosY);
        }//Fin de la boucle

        //on re-v�rifie que la case est bien vide
        if(jeu[unitePosX][unitePosY] == NULL){
            //la case est bien vide

            // on met � null l'ancien emplacement dans jeu
            jeu[startingPosX][startingPosY] = NULL;

            //on rajoute le pointeur de l'unite dans la nouvelle case
            jeu[unitePosX][unitePosY] = unite_deplace;
        }
        else{
            printf("\n ERREUR Deplacement_Unite_J_bas - Joueur 2 (ou pas de deplacement)\n");
        }
}

void Deplacement_Unite(TListePlayer player_1_HAUT, TListePlayer player_2_BAS, TplateauJeu jeu){

    int ia_player; // donne le joueur qui se d�place -> 1 = Joueur HAUT || 2 = j BAS
    //moveSpeed Nbr_Case_deplace; // moveSpeed d'une unite
    //int unitePosX, unitePosY; // coordonn�es de l'unite � d�placer
    Tunite *unite_deplace; //pointeur de l'unite qui se d�place
    Tunite *unite_actual_1_HAUT; //courant unite du joueur 1
    Tunite *unite_actual_2_BAS; // courant unite du joueur 2
    TListePlayer courant_player1_HAUT = player_1_HAUT;
    TListePlayer courant_player2_BAS = player_2_BAS;

    while(!listeVide(courant_player1_HAUT) || !listeVide(courant_player2_BAS)){
    printf("\nboucle deplacement\n");

        if(!listeVide(courant_player1_HAUT)){

            unite_actual_1_HAUT = getPtrData(courant_player1_HAUT);

            if(!listeVide(courant_player2_BAS)){

                unite_actual_2_BAS = getPtrData(courant_player2_BAS);

                if(Can_move(unite_actual_1_HAUT)){
                    //l'unite joueur1_HAUT peut se d�placer

                    //Priorit� de d�placement aux unit�s qui tape le plus rapidement
                    if(!attaquePlusVite(unite_actual_1_HAUT, unite_actual_2_BAS)){
                        // unite player1_HAUT tape plus vite que unite player2_BAS
                        unite_deplace = unite_actual_1_HAUT;
                        courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                        ia_player = 1;
                    }else{
                        // l'unite player2_BAS tape la plus vite
                        unite_deplace = unite_actual_2_BAS;
                        courant_player2_BAS = getptrNextCell(courant_player2_BAS);
                        ia_player = 2;
                    }
                }else{ // l'unite joueur1_HAUT ne peut pas se d�placer
                    // �vite de comparer en boucle une Tour avec une unite d�placable

                    printf("l'unite 1 haut ne peut pas se d�placer, r�sultat can moove = %d\n", Can_move(unite_actual_1_HAUT));
                    courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                    unite_deplace = unite_actual_2_BAS;
                    courant_player2_BAS = getptrNextCell(courant_player2_BAS);
                    ia_player = 2;
                }
            }else{
                //liste joueur2_BAS vide -> on fait jouer le joueur1_HAUT
                printf("liste joueur2_BAS vide -> 1 haut se d�place\n");
                unite_deplace = unite_actual_1_HAUT;
                courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
                ia_player = 1;
            }
        }else{
            //unite player2_BAS se d�place (liste joueur1_HAUT VIDE)
            printf("liste joueur1_HAUT VIDE -> player2_BAS se deplace \n");
            unite_actual_2_BAS = getPtrData(courant_player2_BAS);
            unite_deplace = unite_actual_2_BAS;
            courant_player2_BAS = getptrNextCell(courant_player2_BAS);
            ia_player = 2;
        }

        /*
        D�placement :
            -> Uniquement s'il n'y a pas d'unite � porter

            -> Pas de d�placement vers l'arri�re (ignore unite derri�re non � portee)

            Si vers la gauche : (0 <= X < 5)
                -> d�placement vers diagonale droite (droite ou centre)
                    -> Si X-Y pair ou X-Y impair                -> d�placement vers la droite (X+1)
                    -> Si X-Y pair/impair ou impair/pair        -> d�placement vers le centre (Y+1 ou Y-1)
                                                                -> en fonction du joueur
                                                                    ->J Haut Y+1
                                                                    ->J BAS Y-1

            Si au centre : (X = 5)
                -> d�placement central
                 -> en fonction du joueur
                    ->Haut Y+1
                    ->BAS Y-1

            Si vers la gauche : (5 < X <= 10)
                -> d�placement vers diagonale gauche (gauche ou centre)
                    -> Si X-Y pair ou X-Y impair                -> d�placement vers la gauche (X-1)
                    -> Si X-Y pair/impair ou impair/pair        -> d�placement vers le centre (Y+1 ou Y-1)
                                                                -> en fonction du joueur (haut/bas)
                                                                -> en fonction du joueur
                                                                    ->J Haut Y+1
                                                                    ->J BAS Y-1
        */

        /*/--------------------------- D�placement ------------------------------  /*/

        if(ia_player == 1){
            //le joueur1_HAUT se d�place
            Deplacement_Unite_J_HAUT(unite_deplace, jeu, player_2_BAS);
            //courant_player1_HAUT = getptrNextCell(courant_player1_HAUT);
        }
        else{
            //le joueur2_BAS se d�place
            Deplacement_Unite_J_BAS(unite_deplace, jeu, player_1_HAUT);
            //courant_player2_BAS = getptrNextCell(courant_player2_BAS);
        }


    }//////-- Fin de la boucle --//////

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Fin Phase Deplacement /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


